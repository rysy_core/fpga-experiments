#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_pkg.sv
vlog pwm.sv
vlog pwm_tb.sv

vsim -novopt work.pwm_tb

add wave -position end sim:/pwm_tb/clk
add wave -position end sim:/pwm_tb/rst
add wave -position end sim:/pwm_tb/bus/valid
add wave -position end -hex sim:/pwm_tb/bus/data
add wave -position end -hex sim:/pwm_tb/dut/value
add wave -position end -hex sim:/pwm_tb/dut/count
add wave -position end sim:/pwm_tb/dut/pwm

run -all

wave zoom full
