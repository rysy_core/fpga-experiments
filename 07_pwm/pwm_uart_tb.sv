/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps

module pwm_uart_tb;
	logic clk;
	logic rst;
	logic rtx;
	StreamBus bus(clk, rst);

	initial begin
		clk <= '0;
		forever #625 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#1250 rst <= 1'b1;
	end

	initial begin
		bus.valid <= 1'b0;
		bus.data <= 4'd0;

		for (int i = 0; i <= 255; i+=5) begin
			bus.data <= i;
			bus.valid <= 1'b1;
			@(posedge clk);
			bus.valid <= 1'b0;
			#400us;
		end
		$stop;
	end

	uart_tx uart_tx(
		.bus(bus),
		.tx(rtx));

	pwm_uart dut (
		.clk(clk),
		.rst(rst),
		.rx(rtx),
		.led());

endmodule

`default_nettype wire
