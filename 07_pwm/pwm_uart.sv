/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module pwm_uart  #(
	parameter F = 8000000
) (
	input wire clk,
	input wire rst,
	input wire rx,
	output logic [7:0]led
);
	StreamBus #(.N(8)) bus (.clk(clk), .rst(rst));
	logic pwm_out;

    uart_rx #(.F(F), .BAUD(115200)) urx (
        .rx(rx),
        .bus(bus));

	pwm #(.MAX(255)) dut(
		.bus(bus),
		.ce(1'b1),
		.pwm(pwm_out));

	genvar i;
	generate
		for (i = 0; i < $bits(led); i++) begin : LED_PWM
			always_ff @(posedge bus.clk)
				led[i] <= pwm_out;
		end
	endgenerate

endmodule

`default_nettype wire
