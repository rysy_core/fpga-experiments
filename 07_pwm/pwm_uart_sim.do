#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_pkg.sv
vlog ../05_uart_tx/uart_tx.sv
vlog ../06_uart_rx/uart_rx.sv
vlog pwm.sv
vlog pwm_uart.sv
vlog pwm_uart_tb.sv

vsim -novopt work.pwm_uart_tb

add wave -position end sim:/pwm_uart_tb/clk
add wave -position end sim:/pwm_uart_tb/rst

add wave -position end -dec sim:/pwm_uart_tb/bus/data

add wave -position end sim:/pwm_uart_tb/rtx
add wave -position end sim:/pwm_uart_tb/dut/led\[0\]

run -all

wave zoom full
