## SPDX-License-Identifier: BSD-3-Clause
## Copyright (c) 2019 Rafal Kozik

# 8 MHz 
create_clock -name {clk} -period 125.000 -waveform { 0.000 62.500 } [get_ports { clk }]