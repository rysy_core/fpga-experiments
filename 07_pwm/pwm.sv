/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module pwm #(
	parameter MAX = 255,
	parameter MAX_LOG = $clog2(MAX)
) (
	StreamBus.Slave bus,
	input wire ce,
	output logic pwm
);
	logic [MAX_LOG-1:0] count;
	logic [MAX_LOG-1:0] value;

	assign bus.ready = 1'b1;

	counter #(.N(MAX)) ctx (
		.clk(bus.clk),
		.rst(bus.rst),
		.ce(ce),
		.q(count),
		.ov());

	always_ff @(posedge bus.clk or negedge bus.rst)
		if (!bus.rst)
			value <= '0;
		else if (bus.valid)
			value <= bus.data;

	always_ff @(posedge bus.clk)
		pwm <= (count < value);

endmodule

`default_nettype wire
