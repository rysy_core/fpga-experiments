/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps

module gates_tb;
	logic clk, in1, in2;

	initial begin
		clk = '0;
		forever #62.5ns clk = ~clk;
	end

	initial begin
		in1 = 1'b1;
		in2 = 1'b1;
		#200ns
		in1 = 1'b0;
		in2 = 1'b1;
		#200ns
		in1 = 1'b1;
		in2 = 1'b0;
		#200ns
		in1 = 1'b0;
		in2 = 1'b0;
		#200ns
		$stop;
	end

	gates dut (
		.clk(clk),
		.in1(in1),
		.in2(in2),
		.led1(),
		.led2(),
		.led3()
	);
endmodule

`default_nettype wire
