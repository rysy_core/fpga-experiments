/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module gates (
	input wire clk,
	input wire in1,
	input wire in2,
	output logic led1,
	output logic led2,
	output logic led3
);
	logic in1_n, in2_n;

	always_comb begin
		in1_n = ~in1;
		in2_n = ~in2;
	end

	always_ff @(posedge clk) begin
		led1 <= in1_n & in2_n;
		led2 <= in1_n | in2_n;
		led3 <= in1_n ^ in2_n;
	end
endmodule

`default_nettype wire
