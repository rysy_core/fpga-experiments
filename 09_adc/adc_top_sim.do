#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

set QUARTUS_DIR "C:/intelFPGA_lite/18.1/quartus/eda/sim_lib/"

vlib work

vlog $QUARTUS_DIR/altera_mf.v

vlog pll/pll.v
vlog ../02_counter/counter.sv
vlog adc/adc/simulation/submodules/altera_modular_adc_control_avrg_fifo.v
vlog adc/adc/simulation/submodules/fiftyfivenm_adcblock_primitive_wrapper.v
vlog adc/adc/simulation/submodules/chsel_code_converter_sw_to_hw.v
vlog adc/adc/simulation/submodules/fiftyfivenm_adcblock_top_wrapper.v
vlog adc/adc/simulation/submodules/altera_modular_adc_control_fsm.v
vlog adc/adc/simulation/submodules/altera_modular_adc_control.v
vlog adc/adc/simulation/submodules/adc_modular_adc_0.v
vlog adc/adc/simulation/adc.v
vlog adc_top.sv
vlog adc_top_tb.sv

vsim -L fiftyfivenm_ver -novopt work.adc_top_tb

add wave -position end sim:/adc_top_tb/clk
add wave -position end sim:/adc_top_tb/rst
add wave -position end sim:/adc_top_tb/dut/clk_adc
add wave -position end sim:/adc_top_tb/dut/clk_adc_locked
add wave -position end sim:/adc_top_tb/dut/adc_ready_in
add wave -unsigned -position end sim:/adc_top_tb/dut/adc_channel_in
add wave -position end sim:/adc_top_tb/dut/adc_valid_out
add wave -unsigned -position end sim:/adc_top_tb/dut/adc_channel_out
add wave -unsigned -position end sim:/adc_top_tb/dut/adc_data_out

run 1ms
wave zoom full
