/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

 module adc_top (
     input wire clk,
     input wire rst,
     output logic [7:0]led
 );
	wire clk_adc;
	wire clk_adc_locked;
	wire adc_ready_in;
	wire adc_counter;
	wire [4:0]adc_channel_in;
	wire [4:0]adc_channel_out;
	wire adc_valid_out;
	wire [11:0]adc_data_out;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_adc),
        .locked(clk_adc_locked));

	counter #(.N(2)) ctx_channel (
		.clk(clk),
		.rst(rst),
		.ce(adc_ready_in),
		.q(adc_counter),
		.ov());

	assign adc_channel_in = 5'b1 + adc_counter;

	adc adc_inst (
		.clock_clk(clk),
		.reset_sink_reset_n(rst),
		.adc_pll_clock_clk(clk_adc),
		.adc_pll_locked_export(clk_adc_locked),
		.command_valid(1'b1),
		.command_channel(adc_channel_in),
		.command_startofpacket(1'b0),
		.command_endofpacket(1'b0),
		.command_ready(adc_ready_in),
		.response_valid(adc_valid_out),
		.response_channel(adc_channel_out),
		.response_data(adc_data_out),
		.response_startofpacket(),
		.response_endofpacket());

	always_ff @(posedge clk)
		if(adc_valid_out)
			case (adc_channel_out)
				5'd1: led[7:4] <= adc_data_out[11-:4];
				5'd2: led[3:0] <= adc_data_out[11-:4];
				default: ;
			endcase

 endmodule
 
 `default_nettype wire
 