/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

 module rec #(
     parameter F = 8000000
 ) (
     input wire clk,
	 input wire rst,
	 output tx,
     output logic [7:0]led
 );
	wire clk_adc;
	wire clk_adc_locked;
	wire adc_valid_out;
	wire [11:0]adc_data_out;
	StreamBus bus(clk, rst);
	logic ce_5;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_adc),
        .locked(clk_adc_locked));

	adc adc_inst (
		.clock_clk(clk),
		.reset_sink_reset_n(rst),
		.adc_pll_clock_clk(clk_adc),
		.adc_pll_locked_export(clk_adc_locked),
		.command_valid(1'b1),
		.command_channel(1'b1),
		.command_startofpacket(1'b0),
		.command_endofpacket(1'b0),
		.command_ready(),
		.response_valid(adc_valid_out),
		.response_channel(),
		.response_data(adc_data_out),
		.response_startofpacket(),
		.response_endofpacket());

	always_ff @(posedge clk)
		if(adc_valid_out)
			led <= adc_data_out[11-:8];

	assign bus.data = adc_data_out[11-:8];

	counter #(.N(5)) every5 (
		.clk(clk),
		.rst(rst),
		.ce(adc_valid_out),
		.q(),
		.ov(ce_5));

	assign bus.valid = ce_5 & adc_valid_out;

    uart_tx #( 
        .F(F),
        .BAUD(115200)
    ) uart (
        .bus(bus),
        .tx(tx));

 endmodule
 
 `default_nettype wire
 