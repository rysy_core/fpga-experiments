/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none
 `timescale 100ns / 10ns

 module twiddle_lut_tb;
    parameter K = 9;
    parameter N = 32;
    parameter LOG_N = $clog2(N/2);

    logic clk;
    logic [LOG_N-1:0]i;

    initial begin
        clk <= '0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        for (int ii = 0; ii < N; ii++) begin
            i = ii;
            @(posedge clk);
        end
        $stop;
    end

    twiddle_lut #(.N(N), .K(9)) dut (
        .clk(clk),
        .i(i),
        .W_re(),
        .W_im());

 endmodule

 `default_nettype wire
