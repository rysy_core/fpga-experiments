/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module fft_top_tb;
    //parameter dut.adc_inst.modular_adc_0.control_internal.simfilename_ch1 = "parse/adc_sample.txt";
    //          dut/adc_inst/modular_adc_0/control_internal/simfilename_ch0
    logic clk, rst;
    logic [11:0]adc_data_out;
    logic signed [9:0]data_dc;
    logic signed [7:0]data_fft_in;
    logic signed [7:0]fft_re;
    logic signed [7:0]fft_im;
    logic [3:0]fft_addr;
    logic [7:0]abs[15:0];

    initial begin
		clk <= 1'b0;
		forever #62.5ns clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
        #1250 rst <= 1'b1;
	end

    fft_top dut (
        .clk(clk),
        .rst(rst),
        .led());

    always_ff @(posedge clk) begin
        if (dut.adc_valid_out)
            adc_data_out <= dut.adc_data_out;
        if (dut.bus_dcr.valid)
            data_dc <= dut.bus_dcr.data;
        if (dut.bus_fft.valid)
            data_fft_in <= dut.bus_fft.data;
        if (dut.fft_valid) begin
            fft_re <= dut.fft_re;
            fft_im <= dut.fft_im;
            fft_addr <= dut.fft_addr;
        end
        if (dut.abs_valid)
            abs[dut.abs_addr] <= dut.abs;
    end

endmodule

`default_nettype wire
