/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

parameter real pi = 3.14159265358979323846;

module fft_tb;
    parameter K = 8;
    parameter N = 32;
    parameter LOG_N = $clog2(N);
    logic clk, rst;
    logic signed [K-1:0]d_in_re;
    logic signed [K-1:0]d_in_im;
    logic signed [K-1:0]d_out_re;
    logic signed [K-1:0]d_out_im;
    logic valid_in, valid_out;
    logic [LOG_N-1:0]addr;
    real in_re[N-1:0], in_im[N-1:0];
    real out_re[N-1:0], out_im[N-1:0];
    real out_sim_re, out_sim_im;
    real out_sim1_re, out_sim1_im;
    real W_re, W_im;
    int i = 0;
    real rms;
    real a[N/2:0];
    real A[N/2:0];

    // Fill input values
    initial begin
        A[0] = 10; a[0] = 0;
        A[1] = 40; a[1] = 0;
        A[2] = 10; a[2] = 0;
        A[3] = 25; a[3] = 0;
        for (int i = 0; i < N; i++) begin
            in_re[i] = 0; in_im[i] = 0;
            for (int f = 0; f < N/2; f++)
                in_re[i] += A[f] * $cos(2*pi*f*i/N+a[f]);
        end
    end

    initial begin
        clk <= '0;
        forever #50ns clk <= ~clk;
    end

    initial begin
        valid_in = 1'b0;
        rst = 1'b0;
        #100ns @(negedge clk);
        rst <= 1'b1;
        for (int i = 0; i < 2*N; i++) begin
            if (i < N) begin
                d_in_re = in_re[i];
                d_in_im = in_im[i];
            end
            valid_in = 1'b1;
            @(posedge clk);
            valid_in = 1'b0;
            d_in_re = '0;
            d_in_im = '0;
            @(posedge clk);
        end
        valid_in = 1'b0;
    end

    fft #(.K(K), .N(N)) dut (
        .clk(clk),
        .rst(rst),
        .d_in_re(d_in_re),
        .d_in_im(d_in_im),
        .valid_in(valid_in),
        .d_out_re(d_out_re),
        .d_out_im(d_out_im),
        .addr(addr),
        .valid_out(valid_out));

    always_ff @(posedge clk) begin
        if (valid_out) begin
            out_re[addr] <= d_out_re;
            out_im[addr] <= d_out_im;
            i <= i + 1;
        end
        if (i == N) begin
            rms = 0;
            for (int i = 0; i < N; i++) begin
                if (i == 0) begin
                    out_sim_re = A[0];
                    out_sim_im = 0;
                end else if (i < N/2) begin
                    out_sim_re = A[i] * $cos(a[i]) / 2;
                    out_sim_im = A[i] * $sin(a[i]) / 2;
                end else if (i == N/2) begin
                    out_sim_re = A[i] * $cos(a[i]);
                    out_sim_im = A[i] * $sin(a[i]);
                end else begin
                    out_sim_re = A[N - i] * $cos(a[N - i]) / 2;
                    out_sim_im = A[N - i] * $sin(-a[N - i]) / 2;
                end
                $display("s: %.2f+%.2fj h: %.2f+%.2fj",
                out_sim_re, out_sim_im,
                out_re[i], out_im[i]);
                rms += (out_sim_re-out_re[i])**2 + (out_sim_im-out_im[i])**2;
            end
            rms = $sqrt(rms/N);
            $display("RMS of error is %.1f", rms);
            $stop;
        end
    end

endmodule

`default_nettype wire
