#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog delay.sv
vlog abs.sv
vlog abs_tb.sv

vsim work.abs_tb

add wave -position end sim:/abs_tb/clk
add wave -position end sim:/abs_tb/rst

add wave -divider input
add wave -position end -decimal sim:/abs_tb/dut/d_re
add wave -position end -decimal sim:/abs_tb/dut/d_im
add wave -position end sim:/abs_tb/dut/valid_in
add wave -position end -unsigned sim:/abs_tb/dut/addr_in

add wave -divider middle
add wave -position end -decimal sim:/abs_tb/dut/re_abs
add wave -position end -decimal sim:/abs_tb/dut/im_abs
add wave -position end -decimal sim:/abs_tb/dut/re_max
add wave -position end -decimal sim:/abs_tb/dut/im_max
add wave -position end -decimal sim:/abs_tb/dut/re_sub_im
add wave -position end -decimal sim:/abs_tb/dut/abs1

add wave -divider out
add wave -position end -decimal sim:/abs_tb/dut/abs
add wave -position end sim:/abs_tb/dut/valid_out
add wave -position end -unsigned sim:/abs_tb/dut/addr_out

run -all
wave zoom full
