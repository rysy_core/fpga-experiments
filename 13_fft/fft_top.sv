/*-
* SPDX-License-Identifier: BSD-3-Clause
*
* Copyright (c) 2020 Rafal Kozik
* All rights reserved.
*/

`default_nettype none

module fft_top #(
    parameter F = 8000000,
    parameter LED = 8
) (
    input wire clk,
    input wire rst,
    input wire sw1,
    output logic [LED-1:0]led
);
    parameter N_ADC = 10;
    parameter K = 8;
    parameter N = 16;
    parameter LOG_N = $clog2(N);    

    wire clk_adc;
    wire clk_adc_locked;
    wire adc_valid_out;
    wire [11:0]adc_data_out;
	StreamBus #(.N(N_ADC)) bus_adc(clk, rst);
    StreamBus #(.N(N_ADC)) bus_dcr(clk, rst);
    StreamBus #(.N(K+2)) bus_abs(clk, rst);
    StreamBus #(.N(K)) bus_sat(clk, rst);
    StreamBus #(.N(K)) bus_fft(clk, rst);
    logic signed [K-1:0]fft_re;
    logic signed [K-1:0]fft_im;
    logic [K-1:0]abs;
    logic [LOG_N-1:0]fft_addr;
    logic [LOG_N-1:0]abs_addr;
    logic [LOG_N-1:0]sat_addr;
    logic fft_valid, abs_valid;
    logic ce_5;
    logic sw1_r;

    always_ff @(posedge clk)
        sw1_r <= sw1;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_adc),
        .locked(clk_adc_locked));

    adc adc_inst (
        .clock_clk(clk),
        .reset_sink_reset_n(rst),
        .adc_pll_clock_clk(clk_adc),
        .adc_pll_locked_export(clk_adc_locked),
        .command_valid(1'b1),
        .command_channel(5'b1),
        .command_startofpacket(1'b0),
        .command_endofpacket(1'b0),
        .command_ready(),
        .response_valid(adc_valid_out),
        .response_channel(),
        .response_data(adc_data_out),
        .response_startofpacket(),
        .response_endofpacket());

    counter #(.N(5)) every5 (
		.clk(clk),
		.rst(rst),
		.ce(adc_valid_out),
		.q(),
		.ov(ce_5));
	assign bus_adc.valid = adc_valid_out & ce_5;
    assign bus_adc.data = adc_data_out[11-:N_ADC];

    dc_r #(.N(N_ADC)) dc_rem (
        .in(bus_adc),
        .out(bus_dcr));

    saturation #(.N_IN(N_ADC), .N_OUT(K)) sat (
		.in(bus_dcr),
		.out(bus_fft));

    fft #(.K(K), .N(N)) fft_inst (
        .clk(clk),
        .rst(rst),
        .d_in_re(bus_fft.data),
        .d_in_im('0),
        .valid_in(bus_fft.valid),
        .d_out_re(fft_re),
        .d_out_im(fft_im),
        .addr(fft_addr),
        .valid_out(fft_valid));

    abs #(.K(K), .N(LOG_N)) abs_inst (
        .clk(clk),
        .rst(rst),
        .d_re(fft_re),
        .d_im(fft_im),
        .valid_in(fft_valid),
        .addr_in(fft_addr),
        .abs(abs),
        .addr_out(abs_addr),
        .valid_out(abs_valid));

    assign bus_abs.valid = abs_valid;
    assign bus_abs.data = (sw1 || abs > 10) ? 4 * abs : '0;
    always_ff @(posedge clk)
        sat_addr <= abs_addr;

    saturation #(.N_IN(K+2), .N_OUT(K)) sat1 (
        .in(bus_abs),
        .out(bus_sat));

    generate
        genvar i;        
        for (i = 0; i < LED; i++) begin : PWM
            StreamBus pwm_bus(clk, rst);
            assign pwm_bus.valid = bus_sat.valid & (sat_addr == (i + 1));
            assign pwm_bus.data = bus_sat.data;
            pwm #(.MAX(2**K-1)) pwm_inst (
                .bus(pwm_bus),
                .ce(1'b1),
                .pwm(led[i]));
        end
    endgenerate

endmodule

`default_nettype wire
