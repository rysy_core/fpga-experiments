/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

parameter real pi = 3.14159265358979323846;

module butterfly_tb;
    parameter K = 8;
    parameter N = 8;
    logic clk, rst;
    logic signed [K-1:0]d_in_re;
    logic signed [K-1:0]d_in_im;
    logic signed [K-1:0]d_out_re;
    logic signed [K-1:0]d_out_im;
    logic valid_in, valid_out;
    real in_re[N-1:0], in_im[N-1:0];
    real out_re[N-1:0], out_im[N-1:0];
    real out_sim_re, out_sim_im;
    real out_sim1_re, out_sim1_im;
    real W_re, W_im;
    int i = 0;
    real rms;

    // Fill input values
    initial begin
        in_re[0] = 50;
        in_im[0] = 0;
        in_re[1] = 0;
        in_im[1] = 50;
        in_re[2] = 50;
        in_im[2] = 50;
        in_re[3] = -50;
        in_im[3] = -50;
        in_re[4] = 30;
        in_im[4] = 30;
        in_re[5] = -30;
        in_im[5] = 30;
        in_re[6] = 0;
        in_im[6] = 50;
        in_re[7] = 50;
        in_im[7] = -50;
    end

    initial begin
        clk <= '0;
        forever #50ns clk <= ~clk;
    end

    initial begin
        valid_in = 1'b0;
        rst = 1'b0;
        #100ns @(negedge clk);
        rst <= 1'b1;
        for (int i = 0; i < 3*N/2; i++) begin
            valid_in = 1'b1;
            if (i < N) begin
             d_in_re = in_re[i];
             d_in_im = in_im[i];
            end
            @(posedge clk);
            valid_in = 1'b0;
            d_in_re = '0;
            d_in_im = '0;
            @(posedge clk);
            if (i % 3) @(posedge clk);
        end
        valid_in = 1'b0;
    end

    butterfly #(.K(K), .N(N)) dut (
        .clk(clk),
        .rst(rst),
        .d_in_re(d_in_re),
        .d_in_im(d_in_im),
        .valid_in(valid_in),
        .d_out_re(d_out_re),
        .d_out_im(d_out_im),
        .valid_out(valid_out));

    always_ff @(posedge clk) begin
        if (valid_out) begin
            out_re[i] <= d_out_re;
            out_im[i] <= d_out_im;
            i <= i + 1;
        end
        if (i == N) begin
            rms = 0;
            for (int i = 0; i < N; i++) begin
                if (i < N/2) begin
                    out_sim_re = in_re[i] + in_re[i + N/2];
                    out_sim_im = in_im[i] + in_im[i + N/2];
                end else begin
                    out_sim1_re = in_re[i-N/2] - in_re[i];
                    out_sim1_im = in_im[i-N/2] - in_im[i];
                    W_re = $cos(-2*pi*(i-N/2)/N);
                    W_im = $sin(-2*pi*(i-N/2)/N);
                    out_sim_re = out_sim1_re*W_re - out_sim1_im*W_im;
                    out_sim_im = out_sim1_re*W_im + out_sim1_im*W_re;
                end
                out_sim_re = out_sim_re / 2;
                out_sim_im = out_sim_im / 2;
                $display("s: %.2f+%.2fj h: %.2f+%.2fj",
                  out_sim_re, out_sim_im,
                  out_re[i], out_im[i]);
                rms += (out_sim_re-out_re[i])**2 + (out_sim_im-out_im[i])**2;
            end
            rms = $sqrt(rms/N);
            $display("RMS of error is %.1f", rms);
            $stop;
        end
    end

 endmodule

 `default_nettype wire
