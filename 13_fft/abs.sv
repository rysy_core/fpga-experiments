/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module abs #(
    parameter K = 8,
    parameter N = 2
) (
    input wire clk,
    input wire rst,
    input wire signed [K-1:0]d_re,
    input wire signed [K-1:0]d_im,
    input wire valid_in,
    input wire [N-1:0]addr_in,
    output logic signed [K-1:0]abs,
    output logic [N-1:0]addr_out,
    output logic valid_out
);
    parameter LATENCY = 4;
    logic unsigned [K-1:0]re_abs;
    logic unsigned [K-1:0]im_abs;
    logic unsigned [K:0]re_max;
    logic unsigned [K:0]im_max;
    logic signed [K:0]re_sub_im;
    logic unsigned [K:0]abs1;

    always_ff @(posedge clk) begin
        re_abs <= d_re[K-1] ? -d_re : d_re;
        im_abs <= d_im[K-1] ? -d_im : d_im;
    end

    always_ff @(posedge clk) begin
        re_max <= re_abs + (im_abs >> 1);
        im_max <= (re_abs >> 1) + im_abs;
        re_sub_im <= re_abs - im_abs;
    end

    always_ff @(posedge clk)
        abs1 <= re_sub_im[K] ? im_max : re_max;

    always_ff @(posedge clk)
        abs <= abs1 - (abs1 >> 4);

    delay #(.N(LATENCY), .L(1)) valid (
        .clk(clk),
        .rst(rst),
        .ce(1'b1),
        .in(valid_in),
        .out(valid_out));

    delay #(.N(LATENCY), .L(N)) dut (
        .clk(clk),
        .rst(1'b1),
        .ce(1'b1),
        .in(addr_in),
        .out(addr_out));

endmodule

`default_nettype wire
