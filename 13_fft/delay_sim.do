#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog delay.sv
vlog delay_tb.sv

vsim work.delay_tb

add wave -position end sim:/delay_tb/clk
add wave -position end sim:/delay_tb/rst
add wave -position end sim:/delay_tb/in
add wave -position end sim:/delay_tb/dut/ce
add wave -position end sim:/delay_tb/dut/out

run -all
wave zoom full
