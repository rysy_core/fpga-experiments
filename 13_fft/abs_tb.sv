/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module abs_tb;
    parameter K = 8;
    parameter N = 3;
    parameter LOG_N = $clog2(N);
    parameter C = 5;
    logic clk, rst;
    logic signed [K-1:0]d_re;
    logic signed [K-1:0]d_im;
    logic signed [K-1:0]abs;
    logic [N-1:0] addr_in;
    logic [N-1:0] addr_out;
    logic valid_in, valid_out;
    real in_re[C-1:0], in_im[C-1:0];
    real abs_sim, rms;
    int i;

    initial begin
        in_re[0] = 10; in_im[0] = 20;
        in_re[1] = -10; in_im[1] = -20;
        in_re[2] = 10; in_im[2] = 0;
        in_re[3] = 0; in_im[3] = 20;
        in_re[4] = -10; in_im[4] = 20;
    end

    initial begin
        clk <= '0;
        forever #50ns clk <= ~clk;
    end

    initial begin
        valid_in = 1'b0;
        rst = 1'b0;
        #100ns @(negedge clk);
        rst <= 1'b1;
        valid_in = 1'b1;
        for (int i = 0; i < C; i++) begin
            d_re = in_re[i];
            d_im = in_im[i];
            addr_in = i;
            @(posedge clk);
        end
        valid_in = 1'b0;
    end

    abs #(.K(K), .N(N)) dut (
        .clk(clk),
        .rst(rst),
        .d_re(d_re),
        .d_im(d_im),
        .valid_in(valid_in),
        .addr_in(addr_in),
        .abs(abs),
        .addr_out(addr_out),
        .valid_out(valid_out));

    always_ff @(posedge clk) begin
        if (!rst) begin
            rms = 0;
            i = 0;
        end
        if (valid_out) begin
            abs_sim = $sqrt(in_re[i]**2 + in_im[i]**2);
            $display("s: %.2f, h: %.2f, diff: %.2f", abs_sim, abs, abs_sim - abs);
            rms += $sqrt((abs_sim - abs)**2);
            i <= i + 1;
        end
        if (i == C) begin
            rms = $sqrt(rms/N);
            $display("RMS of error is %.1f", rms);
            $stop;
        end
    end

endmodule

`default_nettype wire
