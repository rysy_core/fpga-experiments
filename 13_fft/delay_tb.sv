/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none
 `timescale 100ns / 10ns

 module delay_tb;
    parameter L = 4;
    parameter N = 4;
    logic clk, rst;
    logic [L-1:0]in;

    initial begin
        clk <= '0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        @(negedge clk);
        rst <= 1'b1;
    end

    initial begin
        for (int i = 0; i < 2**N; i++) begin
            in <= i;
            @(posedge clk);
        end
        $stop;
    end

    delay #(.N(N), .L(L)) dut (
        .clk(clk),
        .rst(rst),
        .ce(1'b1),
        .in(in),
        .out());

 endmodule

 `default_nettype wire
