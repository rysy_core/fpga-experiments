# FPGA experiments

1. https://ep.com.pl/projekty/projekty-ep/13495-rysino-plytka-ewaluacyjna-z-fpga-intel-max10
2. https://ep.com.pl/kursy/13584-eksperymenty-z-fpga-2 https://ep.com.pl/files/bno/12743-132-136.pdf
3. https://ep.com.pl/kursy/14568-eksperymenty-z-fpga-3 https://ep.com.pl/files/nmr/12910-EP_2020-02_131-137.pdf
4. https://ep.com.pl/kursy/14173-eksperymenty-z-fpga-4 https://ep.com.pl/files/coy/12962-ep_2020-03_125-129.pdf
5. https://ep.com.pl/kursy/14131-eksperymenty-z-fpga-5 https://ep.com.pl/files/bsm/12983-ep_2020-04_099-104.pdf
6. https://ep.com.pl/kursy/14239-eksperymenty-z-fpga-6 https://ep.com.pl/files/wmo/12857-EP_2020-05_109-113.pdf
7.
8. https://ep.com.pl/kursy/14530-eksperymenty-z-fpga-8 https://ep.com.pl/files/bvy/12824-EP07-2020_109-114.pdf
9. https://ep.com.pl/kursy/14636-eksperymenty-z-fpga-9 https://ep.com.pl/files/koe/13018-EP_2020-08_103-106.pdf
10. https://ep.com.pl/kursy/14495-eksperymenty-z-fpga-10-podlaczamy-mikrofon https://ep.com.pl/files/hmw/13045-ep_2020-09_105-113.pdf
11. https://ep.com.pl/kursy/14596-eksperymenty-z-fpga-11-operacje-mnozenia-liczb https://ep.com.pl/files/eip/13071-ep_2020-10_109-114.pdf
12. https://ep.com.pl/kursy/14580-eksperymenty-z-fpga-12-dyskretna-transformata-fouriera https://ep.com.pl/files/bhu/12940-EP_2020-11_116-118.pdf
13. https://ep.com.pl/kursy/14523-eksperymenty-z-fpga-13-dyskretna-transformata-fouriera https://ep.com.pl/files/eqy/13099-EP_2020-12_109-114.pdf
14. https://ep.com.pl/kursy/14526-eksperymenty-z-fpga-14-dyskretna-transformata-fouriera https://ep.com.pl/files/qiw/13126-ep_2021-01_093-098.pdf
15. https://ep.com.pl/kursy/14688-eksperymenty-z-fpga-15-fft-przeplywowa-i-iteracyjna https://ep.com.pl/files/cmu/13161-ep_2021-02_097-098.pdf
16. https://ep.com.pl/kursy/14740-eksperymenty-z-fpga-16-pamiec-ram https://ep.com.pl/files/niv/13197-110-113.pdf
17. https://ep.com.pl/kursy/14795-eksperymenty-z-fpga-17-interfejs-vga
18. https://ep.com.pl/kursy/14845-eksperymenty-z-fpga-18-monitor-interfejsu-vga https://ep.com.pl/files/crj/13245-elektronika_praktyczna_2021-05_118-121.pdf
19.
20.
21. https://ep.com.pl/kursy/14988-eksperymenty-z-fpga-21-monitor-jako-wyswietlacz-alfanumeryczny
22. https://ep.com.pl/kursy/15023-eksperymenty-z-fpga-22-terminal-alfanumeryczny

Board available on [AVT](https://sklep.avt.pl/avt5726.html?fbclid=IwAR1AYIGiUBc2YnRXeU4rKx86XCYT-YRUQqFZF2YvC7An3WFgjYAVuyJ_DaE).

