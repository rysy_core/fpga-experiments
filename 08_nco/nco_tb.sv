/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module nco_tb;
	parameter modN = 4;
	parameter ncoL = 1024;
	parameter sinN = 8;
	logic clk, rst;
	StreamBus #(.N(modN)) period (.clk(clk), .rst(rst));
	StreamBus #(.N(sinN)) out (.clk(clk), .rst(rst));

	initial begin
		clk <= 1'b0;
		forever #5 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#15 rst <= 1'b1;
	end

	initial begin
		period.valid = 1'b1;		
		period.data = 5;
		#200000 period.data = 10;
		#200000 period.data = 15;
		#200000 $stop;
	end

	nco #(.modN(modN), .L(ncoL), .sinN(sinN)) dut (
		.ce(1'b1),
		.period(period),
		.out(out));

endmodule

`default_nettype wire
