#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../05_uart_tx/uart_pkg.sv
vlog ../02_counter/counter.sv
vlog counter_mod.sv
vlog sin_lut.sv
vlog nco.sv
vlog nco_tb.sv

vsim -novopt work.nco_tb

add wave -position end sim:/nco_tb/clk
add wave -position end sim:/nco_tb/rst
add wave -position end -unsigned sim:/nco_tb/period/data
add wave -position end -unsigned sim:/nco_tb/dut/angle
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 1023 sim:/nco_tb/dut/angle
add wave -position end -unsigned sim:/nco_tb/out/data
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 255 sim:/nco_tb/out/data

run -all
wave zoom full
