/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module counter_mod #(
	parameter N = 8
) ( 
	StreamBus.Slave mod,
	input wire ce,
	output logic [N-1:0]q,
	output logic ov
);
	logic [N-1:0]mod_pre;
	logic [N-1:0]mod_r;

	assign mod.ready = 1'b1;

	always_ff @(posedge mod.clk or negedge mod.rst)
		if (!mod.rst)
			mod_pre <= '0;
		else if (mod.valid)
			mod_pre <= mod.data - 2'd2;

	always_ff @(posedge mod.clk or negedge mod.rst)
		if (!mod.rst)
			mod_r <= '0;
		else if (ov)
			mod_r <= mod_pre;

	always_ff @(posedge mod.clk or negedge mod.rst)
		if (!mod.rst)
			q <= '0;
		else if (ce)
			if (ov)
				q <= '0;
			else
				q <= q + 1'd1;

	always_ff @(posedge mod.clk or negedge mod.rst)
		if (!mod.rst)
			ov <= '0;
		else if (ce)
			ov <= (q == mod_r);

endmodule

`default_nettype wire
