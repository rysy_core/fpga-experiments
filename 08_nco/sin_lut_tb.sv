/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module sin_lut_tb;
	logic clk;
	logic [9:0]angle;
	logic [11:0]sinus;

	initial begin
		clk <= 1'b0;
		forever #5 clk <= ~clk;
	end

	initial begin
		angle = '0;
		#20
		for (int i = 1; i < 1024; i++)
			@(posedge clk) angle = i;
		#20 $stop;
	end

	sin_lut dut (
		.clk(clk),
		.angle(angle),
		.sinus(sinus));

endmodule

`default_nettype wire
