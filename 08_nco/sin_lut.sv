/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

parameter real pi = 3.14159265358979323846;

module sin_lut #(
	parameter N=12,
	parameter L=1024,
	parameter file_name="sin_lut.mem"
) (
	input wire clk,
	input wire [$clog2(L)-1:0]angle,
	output logic [N-1:0]sinus
);
	logic [N-1:0]ram[L-1:0];

	initial begin
	// synthesis translate_off
		for (int i = 0; i < L; i++)
			ram[i] = $sin(2*pi*i/L)*(2**(N-1)-1)+2**(N-1)-1;
		$writememb(file_name, ram);
	// synthesis translate_on
		$readmemb(file_name, ram);
	end

	always_ff @(posedge clk)
		sinus <= ram[angle];

endmodule

`default_nettype wire
