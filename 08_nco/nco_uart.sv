/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module nco_uart #(
	parameter F = 8000000,
	parameter BAUD = 115200,
	parameter modN = 8,
	parameter ncoL = 1000,
	parameter sinN = 7
) (
	input wire clk,
	input wire rst,
	input wire rx,
	output logic pwma,
	output logic pwmb
);
	StreamBus #(.N(modN)) bus_mod (.clk(clk), .rst(rst));
	StreamBus #(.N(sinN)) bus_sin (.clk(clk), .rst(rst));
	wire pwm;

	uart_rx #(.F(F), .BAUD(BAUD)) urx (
		.rx(rx),
		.bus(bus_mod));

	nco #(.modN(modN), .L(ncoL), .sinN(sinN),
	      .file_name("top_lut.mem")) nco_i (
		.ce(1'b1),
		.period(bus_mod),
		.out(bus_sin));

	pwm #(.MAX(2**sinN-1)) pwm_i (
		.bus(bus_sin),
		.ce(1'b1),
		.pwm(pwm));

	always_ff @(posedge clk) begin
		pwma <= pwm;
		pwmb <= ~pwm;
	end

endmodule

`default_nettype wire
