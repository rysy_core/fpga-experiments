#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog sin_lut.sv
vlog sin_lut_tb.sv

vsim -novopt work.sin_lut_tb

add wave -position end sim:/sin_lut_tb/clk
add wave -unsigned -position end sim:/sin_lut_tb/angle
add wave -format analog-step -unsigned -height 100 -min 0 -max 1023 sim:/sin_lut_tb/angle
add wave -unsigned -position end sim:/sin_lut_tb/sinus
add wave -format analog-step -unsigned -height 100 -min 0 -max 4096 sim:/sin_lut_tb/sinus

run -all
wave zoom full
