/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

 module counter_mod_tb;
    logic clk, rst;
    StreamBus #(.N(4)) mod (.clk(clk), .rst(rst));
 
    initial begin
        clk <= 1'b0;
        forever #5 clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        #15 rst <= 1'b1;
    end

    initial begin
        mod.data = 2;
        #20
        for (int i = 3; i <= 15; i++)
            #150 mod.data = i;
        #200 $stop;
    end

    assign mod.valid = 1'b1;

    counter_mod #(.N(mod.N)) dut (
        .mod(mod),
        .ce(1'b1),
        .q(),
        .ov());

 endmodule
 
 `default_nettype wire
