/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module edge_detector (
	input wire clk,
	input wire rst,
	input wire d,
	output logic q
);
	logic dr;

	always_ff @(posedge clk or negedge rst)
		if (!rst) begin
			dr <= '0;
			q <= '0;
		end else begin
			dr <= d;
			q <= d & ~dr;
		end
endmodule

`default_nettype wire
