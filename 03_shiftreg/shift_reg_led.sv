/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module shift_reg_led #(
	parameter LED_N = 8
) (
	input wire clk,
	input wire rst,
	input wire ce_in,
	input wire d_in,
	output logic [LED_N-1:0]led
);
	logic ce_in_r, d_in_r;
	logic [LED_N-1:0]led_out;
	logic ce, ce_db;

	always_ff @(posedge clk or negedge rst)
		if (!rst) begin
			ce_in_r <= 1'b0;
			d_in_r <= 1'b0;
			led <= 1'b0;
		end else begin
			ce_in_r <= ce_in;
			d_in_r <= d_in;
			led <= led_out;
		end

	debounce #(.N(20)) db (
		.clk(clk),
		.rst(rst),
		.d(ce_in_r),
		.q(ce_db)
	);

	edge_detector ed (
		.clk(clk),
		.rst(rst),
		.d(ce_db),
		.q(ce)
	);

	shift_reg #(.N(LED_N)) sreg (
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.d(d_in_r),
		.q(led_out)
	);
endmodule

`default_nettype wire
