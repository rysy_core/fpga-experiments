#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog shift_reg.sv
vlog shift_reg_tb.sv

vsim -novopt work.shift_reg_tb

add wave -position end sim:/shift_reg_tb/clk
add wave -position end sim:/shift_reg_tb/rst
add wave -position end sim:/shift_reg_tb/ce
add wave -position end sim:/shift_reg_tb/d
add wave -position end sim:/shift_reg_tb/dut/q\[0\]
add wave -position end sim:/shift_reg_tb/dut/q\[1\]
add wave -position end sim:/shift_reg_tb/dut/q\[2\]
add wave -position end sim:/shift_reg_tb/dut/q\[3\]
run -all

wave zoom full
