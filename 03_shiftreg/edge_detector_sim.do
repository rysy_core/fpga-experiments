#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog edge_detector.sv
vlog edge_detector_tb.sv

vsim -novopt work.edge_detector_tb

add wave -position end sim:/edge_detector_tb/clk
add wave -position end sim:/edge_detector_tb/rst
add wave -position end sim:/edge_detector_tb/d
add wave -position end sim:/edge_detector_tb/dut/dr
add wave -position end sim:/edge_detector_tb/dut/q

run 20us

wave zoom full
