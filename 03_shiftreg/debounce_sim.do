#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog debounce.sv
vlog debounce_tb.sv

vsim -novopt work.debounce_tb

add wave -position end sim:/debounce_tb/clk
add wave -position end sim:/debounce_tb/rst
add wave -position end sim:/debounce_tb/d
add wave -position end sim:/debounce_tb/dut/dr
add wave -position end sim:/debounce_tb/dut/sr
add wave -position end -unsigned sim:/debounce_tb/dut/stable_time\[1:0\]
add wave -position end sim:/debounce_tb/dut/stable_time\[2\]
add wave -position end sim:/debounce_tb/dut/q

run -all

wave zoom full
