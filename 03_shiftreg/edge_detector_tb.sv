/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps

module edge_detector_tb;
	logic clk, rst, d;

	initial begin
		clk = 1'b0;
		forever #62.5ns clk = ~clk;
	end

	initial begin
		rst = 1'b0;
		#125ns rst = 1'b1;
	end

	initial begin
		d = 1'b0;
		repeat (2) begin
			repeat(4) @(posedge clk);
			d = 1'b1;
			repeat(4) @(posedge clk);
			d = 1'b0;
		end
		repeat(2) @(posedge clk);
		$stop;
	end

	edge_detector dut (
		.clk(clk),
		.d(d),
		.q()
	);

endmodule

`default_nettype wire
