/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps

module shift_reg_led_tb; 
	logic clk, rst, ce_in, d_in;

	initial begin
		clk = 1'b0;
		forever #62.5ns clk = ~clk;
	end

	initial begin
		rst = 1'b0;
		#125ns rst = 1'b1;
	end

	initial begin
		ce_in = 1'b0;
		forever # 120ms ce_in = ~ce_in;
	end

	initial begin
		ce_in = 1'b1;
		d_in = 1'b0;
		forever begin
			@(posedge ce_in);
			d_in = ~d_in;
		end
	end

	shift_reg_led dut (
		.clk(clk),
		.rst(rst),
		.ce_in(ce_in),
		.d_in(d_in),
		.led()
	);


endmodule

`default_nettype wire
