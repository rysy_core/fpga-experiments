#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_pkg.sv
vlog ../05_uart_tx/uart_tx.sv
vlog uart_rx.sv
vlog inc.sv
vlog uart_inc.sv
vlog uart_inc_tb.sv

vsim -novopt work.uart_inc_tb

add wave -position end sim:/uart_inc_tb/clk
add wave -position end sim:/uart_inc_tb/rst

add wave -position end sim:/uart_inc_tb/bus_tx/data

add wave -position end sim:/uart_inc_tb/rtx
add wave -position end sim:/uart_inc_tb/dut/utx/state
add wave -position end sim:/uart_inc_tb/dut/tx

add wave -position end sim:/uart_inc_tb/bus_rx/data

run 200us

wave zoom full
