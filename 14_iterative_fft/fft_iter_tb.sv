/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

parameter real pi = 3.14159265358979323846;

module fft_iter_tb;
    parameter N = 16;
    parameter K = 8;
    parameter LOG_N = $clog2(N);

    logic clk, wr;
    logic signed [K-1:0]d_in_re;
    logic signed [K-1:0]d_in_im;
    logic [LOG_N-1:0] addr_in;
    logic valid_in, start, ready;
    logic signed [K-1:0]d_out_re;
    logic signed [K-1:0]d_out_im;
    logic [LOG_N-1:0] addr_out;
    logic valid_out, rst;
    logic [K-1:0]data_re[N-1:0];
    logic [K-1:0]data_im[N-1:0];

    real in_re[N-1:0], in_im[N-1:0];
    real out_re[N-1:0], out_im[N-1:0];
    real out_sim_re, out_sim_im;
    real out_sim1_re, out_sim1_im;
    real W_re, W_im;
    int i = 0;
    real rms;
    real a[N/2:0];
    real A[N/2:0];

    // Fill input values
    initial begin
        A[0] = 10; a[0] = 0;
        A[1] = 20; a[1] = pi/2;
        A[2] = 10; a[2] = pi/4;
        A[3] = 25; a[3] = pi/6;
        A[4] = 15; a[4] = pi/3;
        A[5] = 20; a[5] = -pi/6;
        A[6] = 15; a[6] = pi/6;
        A[7] = 10; a[7] = pi/2;
        for (int i = 0; i < N; i++) begin
            in_re[i] = 0; in_im[i] = 0;
            for (int f = 0; f < N/2; f++)
                in_re[i] += A[f] * $cos(2*pi*f*i/N+a[f]);
        end
    end

    initial begin
        clk <= 1'b0;
    forever #5 clk = ~clk;
    end

    initial begin
    rst = 1'b0;
    start = 1'b0;
    @(posedge clk);
    rst = 1'b1;
    valid_in = 1'b1;
        for (int i=0; i<N; i++) begin
            addr_in = i;
        d_in_re = in_re[i];
        d_in_im = in_im[i];
        @(posedge clk);
    end
    valid_in = 1'b0;
    start = 1'b1;
    @(posedge clk);
    start = 1'b0;
    end

    fft_iter #(.K(K), .N(N)) dut (
    .clk(clk),
    .rst(rst),
    .d_in_re(d_in_re),
    .d_in_im(d_in_im),
    .addr_in(addr_in),
    .valid_in(valid_in),
    .start(start),
    .ready(ready),
    .d_out_re(d_out_re),
    .d_out_im(d_out_im),
    .addr_out(addr_out),
    .valid_out(valid_out));

    for (genvar i = 0; i < N; i++)
        assign {data_re[i], data_im[i]} = dut.fft_ram.data[i];

    always_ff @(posedge clk) begin
        if (valid_out) begin
            out_re[addr_out] <= d_out_re;
            out_im[addr_out] <= d_out_im;
            i <= i + 1;
        end
        if (i == N) begin
            rms = 0;
            for (int i = 0; i < N; i++) begin
                if (i == 0) begin
                    out_sim_re = A[0];
                    out_sim_im = 0;
                end else if (i < N/2) begin
                    out_sim_re = A[i] * $cos(a[i]) / 2;
                    out_sim_im = A[i] * $sin(a[i]) / 2;
                end else if (i == N/2) begin
                    out_sim_re = A[i] * $cos(a[i]);
                    out_sim_im = A[i] * $sin(a[i]);
                end else begin
                    out_sim_re = A[N - i] * $cos(-a[N - i]) / 2;
                    out_sim_im = A[N - i] * $sin(-a[N - i]) / 2;
                end
                $display("s: %.2f+%.2fj h: %.2f+%.2fj",
                out_sim_re, out_sim_im,
                out_re[i], out_im[i]);
                rms += (out_sim_re-out_re[i])**2 + (out_sim_im-out_im[i])**2;
            end
            rms = $sqrt(rms/N);
            $display("RMS of error is %.1f", rms);
            $stop;
        end
    end
endmodule

`default_nettype wire
