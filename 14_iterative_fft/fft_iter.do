#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../12_mul/c_mul.sv
vlog ../13_fft/delay.sv
vlog ../13_fft/twiddle_lut.sv
vlog ram.sv
vlog fft_iter.sv
vlog fft_iter_tb.sv

vsim work.fft_iter_tb

add wave -position end sim:/fft_iter_tb/clk
add wave -position end sim:/fft_iter_tb/rst

add wave -divider input
add wave -position end -decimal sim:/fft_iter_tb/dut/d_in_re
add wave -position end -decimal sim:/fft_iter_tb/dut/d_in_im
add wave -position end -unsigned sim:/fft_iter_tb/dut/addr_in
add wave -position end sim:/fft_iter_tb/dut/valid_in
add wave -position end sim:/fft_iter_tb/dut/start
add wave -position end sim:/fft_iter_tb/dut/ready

add wave -divider fft_iter_in
add wave -position end -unsigned sim:/fft_iter_tb/dut/i
add wave -position end -unsigned sim:/fft_iter_tb/dut/n
add wave -position end -unsigned sim:/fft_iter_tb/dut/ram_addr
add wave -position end -unsigned sim:/fft_iter_tb/dut/w_addr

add wave -divider butterfly
add wave -position end -decimal sim:/fft_iter_tb/dut/d_re
add wave -position end -decimal sim:/fft_iter_tb/dut/d_im
add wave -position end -decimal sim:/fft_iter_tb/dut/d1_re
add wave -position end -decimal sim:/fft_iter_tb/dut/d1_im

add wave -position end -decimal sim:/fft_iter_tb/dut/add_re
add wave -position end -decimal sim:/fft_iter_tb/dut/add_im
add wave -position end -decimal sim:/fft_iter_tb/dut/sub_re
add wave -position end -decimal sim:/fft_iter_tb/dut/sub_im

add wave -position end -decimal sim:/fft_iter_tb/dut/W_re
add wave -position end -decimal sim:/fft_iter_tb/dut/W_im
add wave -position end -decimal sim:/fft_iter_tb/dut/c_re
add wave -position end -decimal sim:/fft_iter_tb/dut/c_im

add wave -position end -decimal sim:/fft_iter_tb/dut/add1_re
add wave -position end -decimal sim:/fft_iter_tb/dut/add1_im
add wave -position end -decimal sim:/fft_iter_tb/dut/c1_re
add wave -position end -decimal sim:/fft_iter_tb/dut/c1_im
add wave -position end sim:/fft_iter_tb/dut/sel

add wave -position end -decimal sim:/fft_iter_tb/dut/out_re
add wave -position end -decimal sim:/fft_iter_tb/dut/out_im
add wave -position end -unsigned sim:/fft_iter_tb/dut/ram_addr1
add wave -position end sim:/fft_iter_tb/dut/work1

add wave -divider RAM
add wave -position end -hex sim:/fft_iter_tb/dut/fft_ram/data
add wave -position end -decimal sim:/fft_iter_tb/data_re
add wave -position end -decimal sim:/fft_iter_tb/data_im

add wave -divider output
add wave -position end -decimal sim:/fft_iter_tb/dut/d_out_re
add wave -position end -decimal sim:/fft_iter_tb/dut/d_out_im
add wave -position end -unsigned sim:/fft_iter_tb/dut/addr_out
add wave -position end sim:/fft_iter_tb/dut/valid_out

run 1000
wave zoom full
