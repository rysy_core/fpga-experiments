/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module ram_tb;
    parameter N = 4;
    parameter LOG_N = $clog2(N);
    parameter K = 8;
    parameter LAT = 2;

    logic clk, wr;
    logic [LOG_N-1:0]addr_wr;
    logic [K-1:0]data_wr;
    logic [LOG_N-1:0]addr_rd;
    logic [K-1:0]data_rd;

    initial begin
        clk <= 1'b0;
    forever #5 clk <= ~clk;
    end

    initial begin
    wr = 1'b1;
        for (int i=0; i<N; i++) begin
            addr_wr = i;
        data_wr = i;
        @(posedge clk);
    end
    wr = 1'b0;
    end

    ram #(.N(N), .K(K)) dut (
        .clk(clk),
        .wr(wr),
        .addr_wr(addr_wr),
        .data_wr(data_wr),
        .addr_rd(addr_rd),
        .data_rd(data_rd));

    initial begin
    repeat (5) @(posedge clk);
    for (int i=0; i<N+LAT; i++) begin
        addr_rd = i;
            @(posedge clk);
        if (i >= LAT)
              assert (data_rd == i-LAT)
                  else $display("Wrong val %d != %d", data_rd, i-LAT);
    end
    $stop;
    end

endmodule

`default_nettype wire
