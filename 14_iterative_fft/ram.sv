/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module ram #(
    parameter N = 32,
    parameter LOG_N = $clog2(N),
    parameter K = 9
) (
    input wire clk,
    input wire wr,
    input wire [LOG_N-1:0]addr_wr,
    input wire [K-1:0]data_wr,
    input wire [LOG_N-1:0]addr_rd,
    output logic [K-1:0]data_rd
);
    logic wr_r;
    logic [LOG_N-1:0]addr_wr_r;
    logic [K-1:0]data_wr_r;
    logic [LOG_N-1:0]addr_rd_r;
    logic [K-1:0]data[N-1:0];

    always_ff @(posedge clk) begin
        wr_r <= wr;
        addr_wr_r <= addr_wr;
        data_wr_r <= data_wr;
        addr_rd_r <= addr_rd;
    end

    always_ff @(posedge clk)
        if (wr_r)
            data[addr_wr_r] <= data_wr_r;

    always_ff @(posedge clk)
        data_rd <= data[addr_rd_r];

endmodule

`default_nettype wire
