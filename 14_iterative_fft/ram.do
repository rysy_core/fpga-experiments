#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ram.sv
vlog ram_tb.sv

vsim work.ram_tb

add wave -position end sim:/ram_tb/clk

add wave -divider wr_port
add wave -position end -hex sim:/ram_tb/addr_wr
add wave -position end -hex sim:/ram_tb/data_wr
add wave -position end sim:/ram_tb/wr

add wave -divider rd_port
add wave -position end -hex sim:/ram_tb/dut/data

add wave -divider rd_port
add wave -position end -hex sim:/ram_tb/addr_rd
add wave -position end -hex sim:/ram_tb/data_rd

run -all
wave zoom full

coverage report -assert

