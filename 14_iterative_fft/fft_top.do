#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

#set QUARTUS_DIR "C:/intelFPGA_lite/18.1/quartus/eda/sim_lib/"
set QUARTUS_DIR "/home/rafal/intelFPGA_lite/20.1/quartus/eda/sim_lib"

vlib work

vlog $QUARTUS_DIR/altera_mf.v

vlog ../11_dc_r/pll.v
vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_pkg.sv
vlog ../05_uart_tx/uart_tx.sv
vlog ../07_pwm/pwm.sv
vlog ../11_dc_r/adc/simulation/submodules/altera_modular_adc_control_avrg_fifo.v
vlog ../11_dc_r/adc/simulation/submodules/fiftyfivenm_adcblock_primitive_wrapper.v
vlog ../11_dc_r/adc/simulation/submodules/chsel_code_converter_sw_to_hw.v
vlog ../11_dc_r/adc/simulation/submodules/fiftyfivenm_adcblock_top_wrapper.v
vlog ../11_dc_r/adc/simulation/submodules/altera_modular_adc_control_fsm.v
vlog ../11_dc_r/adc/simulation/submodules/altera_modular_adc_control.v
vlog ../11_dc_r/adc/simulation/submodules/adc_modular_adc_0.v
vlog ../11_dc_r/adc/simulation/adc.v
vlog ../11_dc_r/dc_r.sv
vlog ../11_dc_r/saturation.sv
vlog ../12_mul/c_mul.sv
vlog ../13_fft/delay.sv
vlog ../13_fft/twiddle_lut.sv
vlog ../13_fft/abs.sv
vlog ram.sv
vlog fft_iter.sv
vlog fft_top.sv
vlog ../13_fft/fft_top_tb.sv

vsim -L fiftyfivenm_ver work.fft_top_tb

add wave -position end sim:/fft_top_tb/clk
add wave -position end sim:/fft_top_tb/rst
add wave -position end sim:/fft_top_tb/dut/clk_adc

add wave -unsigned -position end sim:/fft_top_tb/adc_data_out
add wave -position end -format analog-step -decimal -height 100 -min 0 -max 4095 sim:/fft_top_tb/adc_data_out

add wave -decimal -position end sim:/fft_top_tb/data_dc
add wave -position end -format analog-step -decimal -height 100 -min -512 -max 511 sim:/fft_top_tb/data_dc

add wave -decimal -position end sim:/fft_top_tb/data_fft_in
add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/fft_top_tb/data_fft_in
add wave -decimal -position end sim:/fft_top_tb/dut/fft_addr_in
add wave -decimal -position end sim:/fft_top_tb/dut/fft_start

add wave -decimal -position end sim:/fft_top_tb/abs
# add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/fft_top_tb/data_fft_in

run 100ms
wave zoom full
