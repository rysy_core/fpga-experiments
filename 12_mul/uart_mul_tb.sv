/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps
 
module uart_mul_tb;
    logic clk, rst, rtx, trx;
    StreamBus bus_tx(clk, rst);
    StreamBus bus_rx(clk, rst);
    logic [7:0]data[7:0] = {8'd10, 8'd5, 8'd4, 8'd2, 8'd5, 8'd2, 8'd8, 8'd4};

    initial begin
        clk = '0;
        forever #625 clk = ~clk;
    end
 
    initial begin
        rst = 1'b0;
        #1250 rst = 1'b1;
    end
 
	initial begin
        bus_tx.valid = 1'b0;
        #5000;

        for (int i = 0; i < $size(data); i++) begin
            @(negedge clk);
            bus_tx.data = data[i];
            bus_tx.valid = 1'b1;
            @(posedge clk);
            @(posedge clk);
            bus_tx.valid = 1'b0;
            while (!bus_tx.ready)
                @(posedge clk);
        end

        #400us;
        $stop;
	end

    uart_tx #(.F(8000000)) utx(
		.bus(bus_tx),
		.tx(rtx));

    uart_mul #(.F(8000000)) dut (
        .clk(clk),
        .rst(rst),
        .rx(rtx),
        .tx(trx));

    uart_rx #(.F(8000000)) urx(
        .rx(trx),
		.bus(bus_rx));

endmodule
 
`default_nettype wire
