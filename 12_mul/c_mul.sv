/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module c_mul #(
    parameter K = 9,
    parameter K_OUT1 = 8,
    parameter K_OUT2 = 16
) (
    input wire clk,
    input wire rst,
    input wire signed [K-1:0]a_re,
    input wire signed [K-1:0]a_im,
    input wire signed [K-1:0]b_re,
    input wire signed [K-1:0]b_im,
    output logic signed [K_OUT2-K_OUT1:0]c_re,
    output logic signed [K_OUT2-K_OUT1:0]c_im
);
    logic signed [K-1:0]a_re_r;
    logic signed [K-1:0]a_im_r;
    logic signed [K-1:0]b_re_r;
    logic signed [K-1:0]b_im_r;

    logic signed [2*K-1:0] c_re_1;
    logic signed [2*K-1:0] c_re_2;
    logic signed [2*K-1:0] c_im_1;
    logic signed [2*K-1:0] c_im_2;

    logic signed [2*K:0] c_re_tmp;
    logic signed [2*K:0] c_im_tmp;

    always_ff @(posedge clk) begin
        a_re_r <= a_re;
        a_im_r <= a_im;
        b_re_r <= b_re;
        b_im_r <= b_im;
    end

    always_ff @(posedge clk) begin
        c_re_1 <= a_re_r * b_re_r;
        c_re_2 <= a_im_r * b_im_r;
        c_im_1 <= a_re_r * b_im_r;
        c_im_2 <= a_im_r * b_re_r;
    end

    always_ff @(posedge clk) begin
        c_re_tmp <= c_re_1 - c_re_2;
        c_im_tmp <= c_im_1 + c_im_2;
    end

    assign c_re = c_re_tmp[K_OUT2:K_OUT1];
    assign c_im = c_im_tmp[K_OUT2:K_OUT1];
endmodule

`default_nettype wire
