#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_pkg.sv
vlog ../05_uart_tx/uart_tx.sv
vlog ../06_uart_rx/uart_rx.sv
vlog c_mul.sv
vlog uart_mul.sv
vlog uart_mul_tb.sv

vsim work.uart_mul_tb

add wave -position end sim:/uart_mul_tb/clk
add wave -position end sim:/uart_mul_tb/rst

add wave -position end -decimal sim:/uart_mul_tb/bus_tx/data
add wave -position end sim:/uart_mul_tb/bus_tx/valid
add wave -position end sim:/uart_mul_tb/bus_tx/ready
add wave -position end sim:/uart_mul_tb/rtx

add wave -decimal -position end sim:/uart_mul_tb/dut/bus_rx/data
add wave -position end sim:/uart_mul_tb/dut/bus_rx/valid
add wave -decimal -position end sim:/uart_mul_tb/dut/data_in
add wave -decimal -position end sim:/uart_mul_tb/dut/cnt_in/q
add wave -position end sim:/uart_mul_tb/dut/cnt_in_ov

add wave -decimal -position end sim:/uart_mul_tb/dut/c
add wave -position end sim:/uart_mul_tb/dut/valid_d
add wave -position end sim:/uart_mul_tb/dut/cnt_out

add wave -position end sim:/uart_mul_tb/dut/valid_out
add wave -position end sim:/uart_mul_tb/dut/select_out
add wave -decimal -position end sim:/uart_mul_tb/dut/data_out

add wave -decimal -position end sim:/uart_mul_tb/bus_rx/data

run -all
wave zoom full
