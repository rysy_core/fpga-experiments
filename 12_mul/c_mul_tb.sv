/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none
 `timescale 100ns / 10ns

 module c_mul_tb;
    parameter K = 9;
    parameter LATENCY = 3;
    parameter TESTS = 10;
    logic clk, rst;
    logic signed [K-1:0]a_re;
    logic signed [K-1:0]a_im;
    logic signed [K-1:0]b_re;
    logic signed [K-1:0]b_im;
    logic signed [2*K:0]c_re;
    logic signed [2*K:0]c_im;

    int ra_re[LATENCY-1:0];
    int ra_im[LATENCY-1:0];
    int rb_re[LATENCY-1:0];
    int rb_im[LATENCY-1:0];

    initial begin
        clk <= '0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        @(negedge clk);
        rst <= 1'b1;
    end

    initial begin
        int rc_re, rc_im;
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        for (int i = 0; i < TESTS+LATENCY; i++) begin
            @(negedge clk);
            if (i >= LATENCY) begin
                rc_re = ra_re[i%LATENCY]*rb_re[i%LATENCY]-ra_im[i%LATENCY]*rb_im[i%LATENCY];
                rc_im = ra_re[i%LATENCY]*rb_im[i%LATENCY]+ra_im[i%LATENCY]*rb_re[i%LATENCY];
                $display("sim: %d+%dj rtl: %d+%dj",
                    rc_re, rc_im, c_re, c_im);
            end
            rb_re[i%LATENCY] = 100;
            rb_im[i%LATENCY] = 50;
            ra_re[i%LATENCY] = i*511.0/(TESTS-1)-256;
            ra_im[i%LATENCY] = 255-i*511.0/(TESTS-1);

            a_re = ra_re[i%LATENCY];
            a_im = ra_im[i%LATENCY];
            b_re = rb_re[i%LATENCY];
            b_im = rb_im[i%LATENCY];
            @(posedge clk);
        end
        $stop;
    end

    c_mul #(.K(K),.K_OUT1(0), .K_OUT2(2*K)) dut (
        .clk(clk),
        .rst(rst),
        .a_re(a_re),
        .a_im(a_im),
        .b_re(b_re),
        .b_im(b_im),
        .c_re(c_re),
        .c_im(c_im));

 endmodule

 `default_nettype wire
