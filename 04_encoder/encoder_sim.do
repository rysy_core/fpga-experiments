#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../03_shiftreg/debounce.sv
vlog ../03_shiftreg/edge_detector.sv
vlog counter_dir.sv
vlog encoder.sv
vlog encoder_tb.sv

vsim -novopt work.encoder_tb

add wave -position end sim:/encoder_tb/clk
add wave -position end sim:/encoder_tb/rst
add wave -position end sim:/encoder_tb/a
add wave -position end sim:/encoder_tb/b
add wave -position end sim:/encoder_tb/dut/a_d
add wave -position end sim:/encoder_tb/dut/a_e
add wave -position end sim:/encoder_tb/dut/b_d
add wave -position end -unsigned sim:/encoder_tb/dut/led

run -all
wave zoom full
