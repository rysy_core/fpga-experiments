/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module counter_dir #(
	parameter N = 8
) (
	input wire clk,
	input wire rst,
	input wire ce,
	input wire dir,
	output logic [N-1:0]q
);

	always_ff @(posedge clk or negedge rst)
		if (!rst)
			q <= '0;
		else if (ce)
			q <= q + ((dir) ? 1'b1 : -1'b1);
endmodule

`default_nettype wire
