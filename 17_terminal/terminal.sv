/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module terminal #(
  parameter LETTERS_X = 80,
  parameter LETTERS_X_BIT = $clog2(LETTERS_X),
  parameter LETTERS_Y = 25,
  parameter LETTERS_Y_BIT = $clog2(LETTERS_Y),
  parameter LETTER_BIT = 8,
  parameter CHAR_NUM_BIT = $clog2(LETTERS_X) + $clog2(LETTERS_Y)
) (
	StreamBus.Slave uart,
  output logic [LETTERS_X_BIT-1:0]cursor_x,
  output logic [LETTERS_Y_BIT-1:0]cursor_y,
  output logic we,
  output logic [LETTER_BIT-1:0]wdata,
  output logic [CHAR_NUM_BIT-1:0]waddr
);

  parameter logic [7:0] BACKSPACE = 8'h08;
  parameter logic [7:0] NEW_LINE = 8'h0A;
  parameter logic [7:0] RET = 8'h0D;
  parameter logic [7:0] ESC = 8'h1B;
  parameter logic [7:0] DEL = 8'h7F;

  logic [LETTERS_X_BIT-1:0]cursor_x_prev;
  logic [LETTERS_Y_BIT-1:0]cursor_y_prev;

  logic valid_r;
  logic [LETTER_BIT-1:0] data_r;

  logic [LETTERS_X_BIT-1:0]clearing_x;
  logic [LETTERS_Y_BIT-1:0]clearing_y;
  logic clearing;

  logic esc;

  assign uart.ready = 1'd1;

  // Cursor position
  always_ff @(posedge uart.clk)
    if (!uart.rst)
      {cursor_x, cursor_y} <= '0;
    else if (uart.valid) begin
      if (esc) begin
        // Arrows
        if (uart.data == "A") begin
          if (cursor_y == 0)
            cursor_y <= LETTERS_Y-1;
          else
            cursor_y <= cursor_y - 1'd1;
        end else if (uart.data == "B") begin
          if (cursor_y == LETTERS_Y-1)
            cursor_y <= '0;
          else
            cursor_y <= cursor_y + 1'd1;
        end else if (uart.data == "C") begin
          if (cursor_x == LETTERS_X-1) begin
            cursor_x <= '0;
            if (cursor_y == LETTERS_Y-1)
              cursor_y <= '0;
            else
              cursor_y <= cursor_y + 1'd1;
          end else
            cursor_x <= cursor_x + 1'd1;
        end else if (uart.data == "D") begin
          if (cursor_x == '0) begin
            cursor_x <= LETTERS_X-1;
            if (cursor_y == '0)
              cursor_y <= LETTERS_Y-1;
            else
              cursor_y <= cursor_y - 1'd1;
          end else
            cursor_x <= cursor_x - 1'd1;
        end
      // Characters
      end else if (uart.data >= 32 && uart.data <= 126) begin
        if (cursor_x < LETTERS_X-1)
          cursor_x <= cursor_x + 1'd1;
        else begin
          cursor_x <= '0;
          if (cursor_y < LETTERS_Y-1)
            cursor_y <= cursor_y + 1'd1;
          else
            cursor_y <= '0;
        end
      // Delete
      end else if (uart.data == BACKSPACE || uart.data == DEL) begin
        if (cursor_x != '0)
          cursor_x <= cursor_x - 1'd1;
        else begin
          cursor_x <= LETTERS_X - 2'd1;
          if (cursor_y != '0)
            cursor_y <= cursor_y - 1'd1;
          else
            cursor_y <= LETTERS_Y - 2'd1;
        end
      // New line
      end else if (uart.data == NEW_LINE || uart.data == RET) begin
        cursor_x <= '0;
        cursor_y <= (cursor_y < LETTERS_Y-1) ? cursor_y + 1'd1 : '0;
      end
    // Clearing
    end else if (clearing) begin
      cursor_x <= '0;
      cursor_y <= '0;
    end

  // Delay
  always_ff @(posedge uart.clk)
    if(!uart.rst)
      valid_r <= '0;
    else begin
      valid_r <= uart.valid;
      data_r <= uart.data;
      cursor_x_prev <= cursor_x;
      cursor_y_prev <= cursor_y;
   end

  // Escape mode
  always_ff @(posedge uart.clk)
    if (!uart.rst)
      esc <= 1'd0;
    else begin
      if (uart.data == 27)
        esc <= 1'd1;
      else if (esc)
        case (data_r)
        "A", "B", "C", "D", "c": esc <= 1'd0;
        default: esc <= 1'd1;
        endcase
    end

  // Write to memory
  always_ff @(posedge uart.clk)
    if (!uart.rst) begin
      we <= '0;
      clearing <= 1'd0;
      clearing_x <= '0;
      clearing_y <= '0;
    end else if (clearing) begin
      if (clearing_x < LETTERS_X-1)
        clearing_x <= clearing_x + 1'd1;
      else begin
        clearing_x <= '0;
        if (clearing_y < LETTERS_Y-1)
          clearing_y <= clearing_y + 1'd1;
        else begin
          clearing_y <= '0;
          clearing <= '0;
        end
      end
      we <= 1'd1;
      wdata <= '0;
      waddr <= {clearing_x, clearing_y};
    end else if (valid_r) begin
      if (esc) begin
        if (data_r == "c")
          clearing <= 1'd1;
      end else if (data_r >= 32 && data_r <= 126) begin
        wdata <= data_r;
        waddr <= {cursor_x_prev, cursor_y_prev};
        we <= 1'd1;
      end else if (data_r == BACKSPACE || data_r == DEL) begin
        wdata <= '0;
        waddr <= {cursor_x, cursor_y};
        we <= 1'd1;
      end else begin
        we <= '0;
      end
    end else
      we <= '0;

endmodule

`default_nettype wire
