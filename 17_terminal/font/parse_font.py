from font_uefi import font
from PIL import Image 
import PIL 

CHARS = 128
HIGHT = 19
WIDTH = 8
MEM_HIGHT = 32

data = ""
char = 0

str_format = "{{0:0{}b}}\r\n".format(WIDTH)
str_format_nel = "{{0:0{}b}}".format(WIDTH)

for i in range(CHARS):
  if char >= len(font) or i < font[char][0]:
    for j in range(MEM_HIGHT):
        data += str_format.format(0)
  else:
    for j in range(HIGHT):
      c = str_format_nel.format(font[char][2][j])
      data += c[::-1]
      data += "\r\n"
    for j in range(MEM_HIGHT-HIGHT):
      data += str_format.format(0)
    char += 1

f = open("font.mem", "w")
f.write(data)
f.close()

img = Image.new('RGB', (WIDTH*16, HIGHT*8), color = 'black')

char = 0
for y in range(8):
  for x in range(16):
    c = y*16+x
    if char < len(font) and c == font[char][0]:
      for a in range(HIGHT):
        for b in range(WIDTH):
          if font[char][2][a] & (1 << b) != 0:
            img.putpixel((WIDTH*(x+1)-b, HIGHT*y+a), (255, 255, 255))
      char += 1

img.save("font.png")
 
