/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module terminal_show #(
  parameter H = 640,
  parameter V = 480,
  parameter H_MAX = 800,
  parameter V_MAX = 525,
  parameter LETTER_WIDTH = 8,
  parameter LETTER_HIGH = 19,
  parameter LETTER_HIGH_BIT = $clog2(LETTER_HIGH),
  parameter LETTERS_X = 80,
  parameter LETTERS_X_BIT = $clog2(LETTERS_X),
  parameter LETTERS_Y = 25,
  parameter LETTERS_Y_BIT = $clog2(LETTERS_Y),
  parameter LETTER_BIT = 8,
  parameter COLOR = 3'b111,
  parameter H_BIT = $clog2(H_MAX),
  parameter V_BIT = $clog2(V_MAX),
  parameter CHAR_NUM_BIT = $clog2(LETTERS_X) + $clog2(LETTERS_Y),
  parameter FONT_ADDR_WIDTH = 7 + $clog2(LETTER_HIGH),
  parameter CURSOR_BLINK_RATE = 30,
  parameter CURSOR_BLINK_RATE_BIT = $clog2(CURSOR_BLINK_RATE)
) (
  input wire clk,
  input wire rst,

  input wire hsync,
  input wire vsync,
  input wire valid,
  input wire [H_BIT-1:0]hcnt,
  input wire [V_BIT-1:0]vcnt,

  input wire [LETTERS_X_BIT-1:0]cursor_x,
  input wire [LETTERS_Y_BIT-1:0]cursor_y,
  input wire cursor_on,
  input wire we,
  input wire [LETTER_BIT-1:0]wdata,
  input wire [CHAR_NUM_BIT-1:0]waddr,

  output vgaPkg::vga_t vga
);
  logic cnt_letter_v_rst, cnt_letter_v_ce, ov_letter_v, valid_r;
  logic [H_BIT-1:0]hcnt_r;
  logic [H_BIT-1:0]hcnt_rr;
  logic [V_BIT-1:0]vcnt_r;
  logic v_valid, vsync_r, hsync_r, new_frame;
  logic [LETTER_BIT-1:0]current_letter;
  logic [LETTER_WIDTH-1:0]letter_row;
  logic [CHAR_NUM_BIT-1:0]data_addr;
  logic [LETTER_HIGH_BIT-1:0]char_v;
  logic [LETTERS_Y_BIT-1:0]letter_v;
  logic [LETTERS_Y_BIT-1:0]letter_v_d;
  logic [FONT_ADDR_WIDTH-1:0]font_addr;
  logic [LETTERS_X_BIT-1:0]cursor_x_r;
  logic [LETTERS_Y_BIT-1:0]cursor_y_r;
  logic cursor_on_r;
  logic cnt_cursor_blink_ov, cursor_blink, is_cursor;
 
  always_ff @(posedge clk)
    {hcnt_r, vcnt_r} <= {hcnt, vcnt};

  always_ff @(posedge clk)
    {hsync_r, vsync_r} <= {hsync, vsync};

  assign cnt_letter_v_rst = (vcnt_r == V_MAX - 1);
  assign cnt_letter_v_ce = !hsync && hsync_r;
  assign new_frame = !vsync && vsync_r;

  always_ff @(posedge clk)
    if (new_frame)
      {cursor_x_r, cursor_y_r, cursor_on_r} <= {cursor_x, cursor_y, cursor_on};
 
  counter #(.N(LETTER_HIGH)) cnt_letter_v (
    .clk(clk),
    .rst(!cnt_letter_v_rst),
    .ce(cnt_letter_v_ce),
    .q(letter_v),
    .ov(ov_letter_v));

  counter #(.N(LETTERS_Y + 1)) cnt_char_h (
    .clk(clk),
    .rst(!cnt_letter_v_rst),
    .ce(ov_letter_v & cnt_letter_v_ce),
    .q(char_v),
    .ov());

  counter #(.N(CURSOR_BLINK_RATE)) cnt_cursor_blink (
    .clk(clk),
    .rst(rst),
    .ce(new_frame),
    .q(),
    .ov(cnt_cursor_blink_ov));

  always_ff @(posedge clk)
    if (!rst)
      cursor_blink <= '0;
    else if (new_frame && cnt_cursor_blink_ov)
      cursor_blink <= !cursor_blink;

  assign data_addr = {hcnt_r[H_BIT-1:3], char_v};

  simple_dual_port_ram #(
    .DATA_WIDTH(LETTER_BIT),
    .ADDR_WIDTH(CHAR_NUM_BIT),
    .INIT(1),
    .DATA("test_page.mem")
  ) data (
    .clk(clk),
    .waddr(waddr),
    .wdata(wdata),
    .we(we),
    .raddr(data_addr),
    .q(current_letter)); 

  always_ff @(posedge clk)
    letter_v_d <= letter_v;

  assign font_addr = {current_letter, letter_v_d};

  rom #(
    .DATA_WIDTH(LETTER_WIDTH),
    .ADDR_WIDTH(FONT_ADDR_WIDTH),
    .DATA("font/font.mem")
  ) font (
    .clk(clk),
    .addr(font_addr),
    .q(letter_row));

   delay #(
    .N(3),
    .L($bits({valid, v_valid}))
  ) d_valid (
    .clk(clk),
    .rst(rst),
    .ce('1),
    .in({valid, char_v < LETTERS_Y}),
    .out({valid_r, v_valid}));

   delay #(
    .N(2),
    .L($bits(hcnt_r))
  ) d_hcnt (
    .clk(clk),
    .rst(rst),
    .ce('1),
    .in({hcnt_r}),
    .out(hcnt_rr));

   delay #(
    .N(2),
    .L(1)
  ) d_cursor (
    .clk(clk),
    .rst(rst),
    .ce('1),
    .in((hcnt_r[H_BIT-1:3] == cursor_x_r) && (char_v == cursor_y_r) && cursor_on_r),
    .out(is_cursor));

  always_ff @(posedge clk)
    if (valid_r && v_valid) begin
      if (is_cursor && cursor_blink) begin
        if (letter_row[hcnt_rr[2:0]])
          {vga.red, vga.green, vga.blue} <= '0;
        else
          {vga.red, vga.green, vga.blue} <= COLOR;
      end else begin
        if (letter_row[hcnt_rr[2:0]])
          {vga.red, vga.green, vga.blue} <= COLOR;
        else
          {vga.red, vga.green, vga.blue} <= '0;
      end
    end else begin
      {vga.red, vga.green, vga.blue} <= '0;
    end

  delay #(
    .N(4),
    .L($bits({hsync, vsync}))
  ) d_sync (
    .clk(clk),
    .rst(rst),
    .ce('1),
    .in({hsync, vsync}),
    .out({vga.hsync, vga.vsync}));

endmodule

`default_nettype wire
