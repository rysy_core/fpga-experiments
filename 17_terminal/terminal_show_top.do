#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

set QUARTUS_DIR "C:/intelFPGA_lite/18.1/quartus/eda/sim_lib/"
set QUARTUS_DIR "/home/rafal/intelFPGA_lite/20.1/quartus/eda/sim_lib"
set QUARTUS_DIR "C:/intelFPGA_lite/20.1/quartus/eda/sim_lib/"

vlib work

vlog $QUARTUS_DIR/altera_mf.v

vlog ../02_counter/counter.sv
vlog ../13_fft/delay.sv

vlog ../15_VGA/vga_pkg.sv
vlog ../15_VGA/vga.sv
vlog ../15_VGA/pll.v
vlog ../16_PONG/bmp_pkg.sv
vlog ../16_PONG/vga_monitor.sv

vlog memory/rom.sv
vlog memory/simple_dual_port_ram.sv

vlog terminal_show.sv
vlog terminal_show_top.sv
vlog terminal_show_top_tb.sv

vsim work.terminal_show_top_tb

add wave -position end sim:/terminal_show_top_tb/clk
add wave -position end sim:/terminal_show_top_tb/rst
add wave -position end sim:/terminal_show_top_tb/dut/clk_vga

add wave -divider "input"
add wave -position end sim:/terminal_show_top_tb/dut/ts/hsync
add wave -position end sim:/terminal_show_top_tb/dut/ts/vsync
add wave -position end sim:/terminal_show_top_tb/dut/ts/valid
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/hcnt
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/vcnt

add wave -divider "read data"
add wave -position end sim:/terminal_show_top_tb/dut/ts/cnt_letter_v_rst
add wave -position end sim:/terminal_show_top_tb/dut/ts/cnt_letter_v_ce
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/letter_v
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/char_v
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/hcnt_r
add wave -hex -position end sim:/terminal_show_top_tb/dut/ts/data_addr

add wave -divider "read font"
add wave -ASCII -position end sim:/terminal_show_top_tb/dut/ts/current_letter
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/letter_v_d
add wave -hex -position end sim:/terminal_show_top_tb/dut/ts/font_addr

add wave -divider "display"
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/letter_row\[2:0\]
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/hcnt_rr
add wave -unsigned -position end sim:/terminal_show_top_tb/dut/ts/valid_r
add wave -position end sim:/terminal_show_top_tb/dut/vga

run 35ms
wave zoom full

