// Quartus Prime Verilog Template
// Single Port ROM

`default_nettype none

module rom #(
    parameter DATA_WIDTH = 8,
    parameter ADDR_WIDTH = 8,
    parameter DATA = "data.mem"
) (
    input wire clk, 
    input wire [(ADDR_WIDTH-1):0] addr,
    output reg [(DATA_WIDTH-1):0] q
);

    logic [DATA_WIDTH-1:0] rom[2**ADDR_WIDTH-1:0];

    initial
        $readmemb(DATA, rom);

    always @ (posedge clk)
        q <= rom[addr];

endmodule

`default_nettype wire

