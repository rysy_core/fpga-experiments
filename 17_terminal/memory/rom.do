#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog rom.sv
vlog rom_tb.sv

vsim work.rom_tb

add wave -position end sim:/rom_tb/clk
add wave -unsigned -position end sim:/rom_tb/addr
add wave -unsigned -position end sim:/rom_tb/dut/q

run -all
wave zoom full
