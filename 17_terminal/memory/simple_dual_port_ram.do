#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog simple_dual_port_ram.sv
vlog simple_dual_port_ram_tb.sv

vsim work.simple_dual_port_ram_tb

add wave -position end sim:/simple_dual_port_ram_tb/clk
add wave -unsigned -position end sim:/simple_dual_port_ram_tb/waddr
add wave -unsigned -position end sim:/simple_dual_port_ram_tb/wdata
add wave -position end sim:/simple_dual_port_ram_tb/we
add wave -unsigned -position end sim:/simple_dual_port_ram_tb/raddr
add wave -unsigned -position end sim:/simple_dual_port_ram_tb/dut/q

run -all
wave zoom full
