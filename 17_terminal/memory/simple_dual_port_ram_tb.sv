/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module simple_dual_port_ram_tb;
  parameter TEST_DATA_WIDTH = 8;
  parameter TEST_ADDR_WIDTH = 4;
  logic clk, we;
  logic [TEST_ADDR_WIDTH-1:0]raddr;
  logic [TEST_ADDR_WIDTH-1:0]waddr;
  logic [TEST_DATA_WIDTH-1:0]wdata;

  initial begin
    clk <= '0;
    forever #5ns clk = ~clk;
  end

  initial begin
    we = '0;
    waddr = '0;
    wdata = '0;

    for (int i = 0; i < 2**TEST_ADDR_WIDTH-1; i++) begin
      raddr = i;
      @(posedge clk);
    end
    @(posedge clk);

    we = '1;
    for (int i = 0; i < 2**TEST_ADDR_WIDTH-1; i++) begin
      waddr = i;
      wdata = i;
      @(posedge clk);
    end
    we = '0;

    for (int i = 0; i < 2**TEST_ADDR_WIDTH-1; i++) begin
      raddr = i;
      @(posedge clk);
    end
    @(posedge clk);

    $stop;
  end

  simple_dual_port_ram #(
    .DATA_WIDTH(TEST_DATA_WIDTH),
    .ADDR_WIDTH(TEST_ADDR_WIDTH),
    .INIT(1),
    .DATA("rom_tb.mem")
  ) dut (
    .clk(clk),
    .waddr(waddr),
    .wdata(wdata),
    .we(we),
    .raddr(raddr),
    .q());

endmodule

`default_nettype wire
