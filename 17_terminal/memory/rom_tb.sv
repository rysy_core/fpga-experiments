/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module rom_tb;
  parameter TEST_DATA_WIDTH = 8;
  parameter TEST_ADDR_WIDTH = 4;
  logic clk;
  logic [TEST_ADDR_WIDTH-1:0]addr;

  initial begin
    clk <= '0;
    forever #5ns clk = ~clk;
  end

  initial begin
    for (int i = 0; i < 2**TEST_ADDR_WIDTH-1; i++) begin
      addr = i;
      @(posedge clk);
    end

    @(posedge clk);

    $stop;
  end

  rom #(
    .DATA_WIDTH(TEST_DATA_WIDTH),
    .ADDR_WIDTH(TEST_ADDR_WIDTH),
    .DATA("rom_tb.mem")
  ) dut (
    .clk(clk),
    .addr(addr),
    .q());

endmodule

`default_nettype wire
