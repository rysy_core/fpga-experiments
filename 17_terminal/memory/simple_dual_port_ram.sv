`default_nettype none

module simple_dual_port_ram #(
  parameter DATA_WIDTH = 8,
  parameter ADDR_WIDTH = 6,
  parameter INIT = 0,
  parameter DATA = "data.mem"
) ( 
  input wire clk,
  input wire [ADDR_WIDTH-1:0] waddr,
  input wire [DATA_WIDTH-1:0] wdata, 
  input wire we,
  input wire [ADDR_WIDTH-1:0] raddr,
  output logic [DATA_WIDTH-1:0] q
);
  logic [DATA_WIDTH-1:0] ram[2**ADDR_WIDTH-1:0];

  initial
    if (INIT != 0)
      $readmemb(DATA, ram);

  always_ff @(posedge clk) begin
    if(we)
      ram[waddr] <= wdata;
    q <= ram[raddr];
  end

endmodule

`default_nettype wire

