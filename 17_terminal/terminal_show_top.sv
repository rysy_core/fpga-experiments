/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module terminal_show_top #(
    parameter H = 800,
    parameter V = 525,
    parameter D_H = 640,
    parameter H_V = 480,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire cursor,
    output vgaPkg::vga_t vga
);
    logic [H_BIT-1:0]hcnt_vga;
    logic [V_BIT-1:0]vcnt_vga;

    logic clk_vga, valid_vga, hsync_vga, vsync_vga;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

    terminal_show ts (
      .clk(clk_vga),
      .rst(rst),

      .vsync(vsync_vga),
      .hsync(hsync_vga),
      .valid(valid_vga),
      .hcnt(hcnt_vga),
      .vcnt(vcnt_vga),

      .cursor_x('0),
      .cursor_y('0),
      .cursor_on(cursor),
      .we('0),
      .wdata('0),
      .waddr('0),

      .vga(vga));

endmodule

`default_nettype wire
