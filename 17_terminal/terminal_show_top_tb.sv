/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps/10ps

module terminal_show_top_tb;
    logic clk, rst;
    vgaPkg::vga_t vga;

    initial begin
        clk <= 1'b0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        repeat(6) @(posedge clk);
        rst <= 1'b1;
    end

    terminal_show_top dut (
        .clk(clk),
        .rst(rst),
        .vga(vga));

    vga_monitor #(
        .NAME("test_page")
    ) monitor (
        .clk(dut.clk_vga),
        .rst(rst),
        .c({vga.red, vga.green, vga.blue}),
        .hsync(vga.hsync),
        .vsync(vga.vsync));

endmodule

`default_nettype wire
