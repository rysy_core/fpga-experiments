/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps/10ps

module terminal_tb;
	import uartPkg::*;
  parameter LETTERS_X = 80;
  parameter LETTERS_X_BIT = $clog2(LETTERS_X);
  parameter LETTERS_Y = 25;
  parameter LETTERS_Y_BIT = $clog2(LETTERS_Y);
  parameter LETTER_BIT = 8;
  parameter CHAR_NUM_BIT = $clog2(LETTERS_X) + $clog2(LETTERS_Y);

  logic clk, rst, we;
  logic [LETTERS_X_BIT-1:0]cursor_x;
  logic [LETTERS_Y_BIT-1:0]cursor_y;
  logic [LETTER_BIT-1:0]wdata;
  logic [CHAR_NUM_BIT-1:0]waddr;

  StreamBus bus_uart(clk, rst);

  function automatic void check_ram
    (logic [CHAR_NUM_BIT-1:0]addr, logic [LETTER_BIT-1:0]val);
      assert (data.ram[addr] == val)
      else $display("data[%d] is not %d, (%d, %c)",
        addr, val, data.ram[addr], data.ram[addr]);
  endfunction

  function automatic void check_cursor
    (logic [LETTERS_X_BIT-1:0]x, logic [LETTERS_Y_BIT-1:0]y);
    assert (cursor_x == x && cursor_y == y)
    else $display("Cursor is not (%d, %d), but (%d, %d)",
      x, y, cursor_x, cursor_y);
  endfunction

  initial begin
    clk <= 1'b0;
    forever #62.5ns clk <= ~clk;
  end

  initial begin
    rst <= 1'b0;
    repeat(6) @(posedge clk);
    rst <= 1'b1;
  end

  initial begin
    bus_uart.valid = '0;
    @(posedge rst);
    @(posedge clk);

    $display("RESET");
    check_cursor(0, 0);

    assert (we == 0)
    else $display("we is not 0 after rst (%d)", we);

    bus_uart.data = 50;
    bus_uart.valid = 1;
    @(posedge clk);
    bus_uart.valid = 0;

    repeat(5) @(posedge clk);

    $display("WRITE");
    check_cursor(1, 0);
    check_ram(0, 50);

    bus_uart.valid = 1;
    for (int i = 33; i < 127; i++) begin
      bus_uart.data = i;
      @(posedge clk);
    end
    bus_uart.valid = 0;

    repeat(5) @(posedge clk);

    $display("WRITE MULTIPLE");
    check_cursor(15, 1);
    check_ram({7'd14, 5'd1}, 126);

    bus_uart.valid = 1;
    bus_uart.data = 8;
    @(posedge clk);
    bus_uart.valid = 0;

    repeat(5) @(posedge clk);

    $display("BACKSPACE");
    check_cursor(14, 1);
    check_ram({7'd14, 5'd1}, 0);

    bus_uart.data = 8;
    bus_uart.valid = 1;
    repeat(14) @(posedge clk);
    bus_uart.valid = 0;

    repeat(5) @(posedge clk);

    $display("BACKSPACE MULTIPLE");
    check_cursor(0, 1);
    check_ram({7'd0, 5'd1}, 0);

    bus_uart.valid = 1;
    bus_uart.data = 8;
    @(posedge clk);
    bus_uart.valid = 0;

    repeat(5) @(posedge clk);

    $display("BACKSPACE MULTIPLE 2");
    check_cursor(79, 0);
    check_ram({7'd79, 5'd0}, 0);

    bus_uart.valid = 1;
    bus_uart.data = 10;
    @(posedge clk);
    bus_uart.valid = 0;

    repeat(5) @(posedge clk);

    $display("NEW LINE");
    check_cursor(0, 1);

    bus_uart.valid = 1;
    bus_uart.data = 27;
    @(posedge clk);
    bus_uart.data = "c";
    @(posedge clk);
    bus_uart.valid = 0;

    repeat(2005) @(posedge clk);

    $display("CLEAR");
    check_cursor(0, 0);
    for (logic [6:0] i = 0; i < 80; i++)
      for (logic [4:0] j = 0; j < 20; j++)
        check_ram({i, j}, 0);

    bus_uart.valid = 1;
    bus_uart.data = 27;
    @(posedge clk);
    bus_uart.data = "[";
    @(posedge clk);
    bus_uart.data = "B";
    @(posedge clk);
    bus_uart.valid = 0;
    
    repeat(5) @(posedge clk);

    $display("ARROW DOWN");
    check_cursor(0, 1);
            
    bus_uart.valid = 1;
    bus_uart.data = 27;
    @(posedge clk);
    bus_uart.data = "[";
    @(posedge clk);
    bus_uart.data = "A";
    @(posedge clk);
    bus_uart.valid = 0;
    
    repeat(5) @(posedge clk);

    $display("ARROW UP");
    check_cursor(0, 0);

    bus_uart.valid = 1;
    bus_uart.data = 27;
    @(posedge clk);
    bus_uart.data = "[";
    @(posedge clk);
    bus_uart.data = "C";
    @(posedge clk);
    bus_uart.valid = 0;
    
    repeat(5) @(posedge clk);

    $display("ARROW RIGHT");
    check_cursor(1, 0);

    bus_uart.valid = 1;
    bus_uart.data = 27;
    @(posedge clk);
    bus_uart.data = "[";
    @(posedge clk);
    bus_uart.data = "D";
    @(posedge clk);
    bus_uart.valid = 0;
    
    repeat(5) @(posedge clk);

    $display("ARROW LEFT");
    check_cursor(0, 0);
 
    $stop;
  end

  terminal dut (
    .uart(bus_uart),
    .cursor_x(cursor_x),
    .cursor_y(cursor_y),
    .we(we),
    .wdata(wdata),
    .waddr(waddr));

  simple_dual_port_ram #(
    .DATA_WIDTH(LETTER_BIT),
    .ADDR_WIDTH(CHAR_NUM_BIT),
    .INIT(1),
    .DATA("test_page.mem")
  ) data (
    .clk(clk),
    .waddr(waddr),
    .wdata(wdata),
    .we(we),
    .raddr('0),
    .q()); 

endmodule

`default_nettype wire
