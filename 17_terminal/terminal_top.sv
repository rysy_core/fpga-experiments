/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module terminal_top #(
    parameter H = 800,
    parameter V = 525,
    parameter D_H = 640,
    parameter H_V = 480,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire cursor,
    input wire rx,
    output vgaPkg::vga_t vga
);
  parameter LETTERS_X = 80;
  parameter LETTERS_X_BIT = $clog2(LETTERS_X);
  parameter LETTERS_Y = 25;
  parameter LETTERS_Y_BIT = $clog2(LETTERS_Y);
  parameter LETTER_BIT = 8;
  parameter CHAR_NUM_BIT = $clog2(LETTERS_X) + $clog2(LETTERS_Y);

   logic [H_BIT-1:0]hcnt_vga;
   logic [V_BIT-1:0]vcnt_vga;

   logic [LETTERS_X_BIT-1:0]cursor_x;
   logic [LETTERS_Y_BIT-1:0]cursor_y;
   logic [LETTER_BIT-1:0]wdata;
   logic [CHAR_NUM_BIT-1:0]waddr;
   logic we;
   logic clk_vga, valid_vga, hsync_vga, vsync_vga;

   StreamBus bus_uart(clk_vga, rst);

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

  	uart_rx #(.F(25200000)) uart_rx (
		  .rx(rx),
		  .bus(bus_uart));

    terminal t (
      .uart(bus_uart),
      .cursor_x(cursor_x),
      .cursor_y(cursor_y),
      .we(we),
      .wdata(wdata),
      .waddr(waddr));

    terminal_show ts (
      .clk(clk_vga),
      .rst(rst),

      .vsync(vsync_vga),
      .hsync(hsync_vga),
      .valid(valid_vga),
      .hcnt(hcnt_vga),
      .vcnt(vcnt_vga),

      .cursor_x(cursor_x),
      .cursor_y(cursor_y),
      .cursor_on(cursor),
      .we(we),
      .wdata(wdata),
      .waddr(waddr),

      .vga(vga));

endmodule

`default_nettype wire
