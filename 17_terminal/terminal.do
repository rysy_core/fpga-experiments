#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog memory/simple_dual_port_ram.sv
vlog ../05_uart_tx/uart_pkg.sv

vlog terminal.sv
vlog terminal_tb.sv

vsim work.terminal_tb

add wave -position end sim:/terminal_tb/rst
add wave -position end sim:/terminal_tb/clk

add wave -divider "input"
add wave -position end sim:/terminal_tb/bus_uart.data
add wave -position end sim:/terminal_tb/bus_uart.valid

add wave -divider "inside"
add wave -unsigned -position end sim:/terminal_tb/dut/clearing_x
add wave -unsigned -position end sim:/terminal_tb/dut/clearing_y
add wave -position end sim:/terminal_tb/dut/clearing
add wave -position end sim:/terminal_tb/dut/esc

add wave -divider "output"
add wave -unsigned -position end sim:/terminal_tb/cursor_x
add wave -unsigned -position end sim:/terminal_tb/cursor_y
add wave -unsigned -position end sim:/terminal_tb/waddr
add wave -unsigned -position end sim:/terminal_tb/wdata
add wave -unsigned -position end sim:/terminal_tb/we

run -all
wave zoom full

coverage report -assert
