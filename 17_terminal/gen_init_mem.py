W = 80
H = 25
MEM_W = 128
MEM_H = 32
C_MAX = 128
WIDTH = 8

str_format = "{{0:0{}b}}\r\n".format(WIDTH)

f = open("test_page.mem", "w")

for w in range(MEM_W):
  for h in range(MEM_H):
    if (h == 0 or h == H-1) and w < W:
      f.write(str_format.format(ord('!') + w))
    elif w < W and h < H:
      c = W * (h % 2) + w
      f.write(str_format.format(c if c < C_MAX else 0))
    else:
      f.write(str_format.format(0))

f.close()

