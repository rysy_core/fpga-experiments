/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

parameter real pi = 3.14159265358979323846;

module truncation_saturation;
	parameter N = 8;
    logic signed [N-1:0]data;
    logic signed [N-2:0]data_t;
    logic signed [N-2:0]data_s;

    initial begin
        for (int i = 0; i < 100; i++) begin
            #1;
            data = 80*$sin(2*pi*i/100);
            data_t = data[N-2:0];
            if (data > 2**(N-2)-1)
                data_s = 2**(N-2)-1;
            else if (data < -(2**(N-2)))
                data_s = -2**(N-2);
            else
                data_s = data[N-2:0];
        end
        $stop;
    end
endmodule

`default_nettype wire
