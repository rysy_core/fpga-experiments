/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

parameter real pi = 3.14159265358979323846;

module saturation_tb;
    parameter N_IN = 10;
    parameter N_OUT = 8;

	logic clk, rst;
	StreamBus #(.N(N_IN)) in (.clk(clk), .rst(rst));
	StreamBus #(.N(N_OUT)) out (.clk(clk), .rst(rst));

    initial begin
		clk <= 1'b0;
		forever #1 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#4 rst <= 1'b1;
	end

    initial begin
        in.valid = 1'b1;
        for (int i = -2**(N_IN-1); i < 2**(N_IN-1); i++) begin
            @(posedge clk);
            in.data = i;
        end
        $stop;
    end

    saturation dut (
        .in(in),
        .out(out)
    );
endmodule

`default_nettype wire
