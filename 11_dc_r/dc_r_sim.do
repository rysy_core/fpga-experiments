#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../05_uart_tx/uart_pkg.sv
vlog dc_r.sv
vlog dc_r_tb.sv

vsim -novopt work.dc_r_tb

add wave -position end sim:/dc_r_tb/clk
add wave -position end sim:/dc_r_tb/rst
add wave -position end sim:/dc_r_tb/in/valid
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 1023 sim:/dc_r_tb/in/data
add wave -position end -unsigned sim:/dc_r_tb/in/data

add wave -position end -format analog-step -decimal -height 100 -min -1024 -max 1023 sim:/dc_r_tb/dut/x
add wave -position end -format analog-step -decimal -height 100 -min -16384 -max 16383 sim:/dc_r_tb/dut/sum

add wave -position end -format analog-step -decimal -height 100 -min -8192 -max 8191 sim:/dc_r_tb/dut/s

add wave -position end sim:/dc_r_tb/out/valid
add wave -position end -decimal sim:/dc_r_tb/out/data
add wave -position end -format analog-step -decimal -height 100 -min -512 -max 511 sim:/dc_r_tb/out/data
run -all
wave zoom full
