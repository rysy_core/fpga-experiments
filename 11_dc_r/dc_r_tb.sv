/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

parameter real pi = 3.14159265358979323846;

module dc_r_tb;
	parameter N = 10;
	logic clk, rst;
	StreamBus #(.N(N)) in (.clk(clk), .rst(rst));
	StreamBus #(.N(N)) out (.clk(clk), .rst(rst));

    initial begin
		clk <= 1'b0;
		forever #1 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#4 rst <= 1'b1;
	end

    initial begin
        in.valid = 1'b1;
        for (int i = 0; i < 1000; i++) begin
            @(posedge clk);
            in.data = 120*$sin(2*pi*i/100)+480;
        end
        $stop;
    end

    dc_r #(.N(N), .S(5)) dut (
        .in(in),
        .out(out));
endmodule

`default_nettype wire
