/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

 module rec #(
     parameter F = 8000000
 ) (
     input wire clk,
	 input wire rst,
	 output logic tx,
     output logic [7:0]led
 );
	parameter N_ADC = 10;
	parameter N = 8;

	wire clk_adc;
	wire clk_adc_locked;
	wire adc_valid_out;
	wire [11:0]adc_data_out;
	logic ce_5;
	StreamBus #(.N(N_ADC)) bus_adc(clk, rst);
	StreamBus #(.N(N_ADC)) bus_dc(clk, rst);
    StreamBus #(.N(N)) bus_uart(clk, rst);

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_adc),
        .locked(clk_adc_locked));

	adc adc_inst (
		.clock_clk(clk),
		.reset_sink_reset_n(rst),
		.adc_pll_clock_clk(clk_adc),
		.adc_pll_locked_export(clk_adc_locked),
		.command_valid(1'b1),
		.command_channel(5'b1),
		.command_startofpacket(1'b0),
		.command_endofpacket(1'b0),
		.command_ready(),
		.response_valid(adc_valid_out),
		.response_channel(),
		.response_data(adc_data_out),
		.response_startofpacket(),
		.response_endofpacket());

	always_ff @(posedge clk)
		if(bus_uart.valid)
			led <= bus_uart.data;

	assign bus_adc.data = adc_data_out[11-:N_ADC];

	counter #(.N(5)) every5 (
		.clk(clk),
		.rst(rst),
		.ce(adc_valid_out),
		.q(),
		.ov(ce_5));
	assign bus_adc.valid = adc_valid_out & ce_5;

    dc_r #(.N(N_ADC)) dc_remove (
        .in(bus_adc),
        .out(bus_dc));

	saturation #(.N_IN(N_ADC), .N_OUT(N)) sat (
		.in(bus_dc),
		.out(bus_uart));

    uart_tx #( 
        .F(F),
        .BAUD(115200)
    ) uart (
        .bus(bus_uart),
        .tx(tx));

 endmodule
 
 `default_nettype wire
 