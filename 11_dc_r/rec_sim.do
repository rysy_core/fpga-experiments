#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

set QUARTUS_DIR "C:/intelFPGA_lite/18.1/quartus/eda/sim_lib/"

vlib work

vlog $QUARTUS_DIR/altera_mf.v

vlog pll.v
vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_pkg.sv
vlog ../05_uart_tx/uart_tx.sv
vlog adc/simulation/submodules/altera_modular_adc_control_avrg_fifo.v
vlog adc/simulation/submodules/fiftyfivenm_adcblock_primitive_wrapper.v
vlog adc/simulation/submodules/chsel_code_converter_sw_to_hw.v
vlog adc/simulation/submodules/fiftyfivenm_adcblock_top_wrapper.v
vlog adc/simulation/submodules/altera_modular_adc_control_fsm.v
vlog adc/simulation/submodules/altera_modular_adc_control.v
vlog adc/simulation/submodules/adc_modular_adc_0.v
vlog adc/simulation/adc.v
vlog dc_r.sv
vlog saturation.sv
vlog rec.sv
vlog rec_tb.sv

vsim -L fiftyfivenm_ver -novopt work.rec_tb

add wave -position end sim:/rec_tb/clk
add wave -position end sim:/rec_tb/rst
add wave -position end sim:/rec_tb/dut/clk_adc

add wave -unsigned -position end sim:/rec_tb/adc_data_out
add wave -position end -format analog-step -decimal -height 100 -min 0 -max 4095 sim:/rec_tb/adc_data_out

add wave -decimal -position end sim:/rec_tb/data_dc
add wave -position end -format analog-step -decimal -height 100 -min -512 -max 511 sim:/rec_tb/data_dc

add wave -decimal -position end sim:/rec_tb/data_out
add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/rec_tb/data_out

run 100ms
wave zoom full
