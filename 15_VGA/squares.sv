/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module squares #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire hsync_in,
    input wire vsync_in,
    input wire valid_in,
    input wire [H_BIT-1:0]hcnt_in,
    input wire [V_BIT-1:0]vcnt_in,
    output logic hsync,
    output logic vsync,
    output logic red,
    output logic green,
    output logic blue
);

    always_ff @(posedge clk)
        if (!rst) begin
            hsync <= '0;
            vsync <= '0;
            red <= '0;
            green <= '0;
            blue <= '0;
        end else begin
            hsync <= hsync_in;
            vsync <= vsync_in;
            if (valid_in) begin
                red <= vcnt_in < 240;
                if (hcnt_in < 160)
                    {green, blue} <= {1'b0, 1'b0};
                else if (hcnt_in < 320)
                    {green, blue} <= {1'b0, 1'b1};
                else if (hcnt_in < 480)
                    {green, blue} <= {1'b1, 1'b0};
                else
                    {green, blue} <= {1'b1, 1'b1};
            end else
                {red, green, blue} <= '0;
        end

endmodule

`default_nettype wire
