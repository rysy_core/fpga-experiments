/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module vga_top1 #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire s,
    output logic hsync,
    output logic vsync,
    output logic red,
    output logic green,
    output logic blue
);
    logic [H_BIT-1:0]hcnt_vga;
    logic [V_BIT-1:0]vcnt_vga;
    logic clk_vga, valid_vga, hsync_vga, vsync_vga;
    vgaPkg::vga_t vga_s, vga_f;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

    squares #(.H(H), .V(V)) squares_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync_in(hsync_vga),
        .vsync_in(vsync_vga),
        .valid_in(valid_vga),
        .hcnt_in(hcnt_vga),
        .vcnt_in(vcnt_vga),
        .hsync(vga_s.hsync),
        .vsync(vga_s.vsync),
        .red(vga_s.red),
        .green(vga_s.green),
        .blue(vga_s.blue));
    
    from_mem #(.H(H), .V(V)) fm_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync_in(hsync_vga),
        .vsync_in(vsync_vga),
        .valid_in(valid_vga),
        .hcnt_in(hcnt_vga),
        .vcnt_in(vcnt_vga),
        .vga(vga_f));

    always_ff @(posedge clk_vga) begin
        hsync <= s ? vga_s.hsync : vga_f.hsync;
        vsync <= s ? vga_s.vsync : vga_f.vsync;
        red <= s ? vga_s.red : vga_f.red;
        green <= s ? vga_s.green : vga_f.green;
        blue <= s ? vga_s.blue : vga_f.blue;
    end

endmodule

`default_nettype wire
