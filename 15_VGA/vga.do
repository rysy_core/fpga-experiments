#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog vga.sv
vlog vga_tb.sv

vsim work.vga_tb

add wave -position end sim:/vga_tb/clk
add wave -position end sim:/vga_tb/rst

add wave -divider out

add wave -position end sim:/vga_tb/dut/hsync
add wave -position end sim:/vga_tb/dut/vsync
add wave -position end sim:/vga_tb/dut/valid
add wave -position end -unsigned sim:/vga_tb/dut/hcnt
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 799 sim:/vga_tb/dut/hcnt
add wave -position end -unsigned sim:/vga_tb/dut/vcnt
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 524 sim:/vga_tb/dut/vcnt

run 20ms
wave zoom full
