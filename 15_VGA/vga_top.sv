/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module vga_top #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire s,
    output logic hsync,
    output logic vsync,
    output logic red,
    output logic green,
    output logic blue
);
    logic [H_BIT-1:0]hcnt_vga;
    logic [V_BIT-1:0]vcnt_vga;
    logic clk_vga, valid_vga, hsync_vga, vsync_vga;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

    squares #(.H(H), .V(V)) squares_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync_in(hsync_vga),
        .vsync_in(vsync_vga),
        .valid_in(valid_vga),
        .hcnt_in(hcnt_vga),
        .vcnt_in(vcnt_vga),
        .hsync(hsync),
        .vsync(vsync),
        .red(red),
        .green(green),
        .blue(blue));

endmodule

`default_nettype wire
