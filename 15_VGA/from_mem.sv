/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module from_mem #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V),
    parameter NAME = "test.mem"
) (
    input wire clk,
    input wire rst,
    input wire hsync_in,
    input wire vsync_in,
    input wire valid_in,
    input wire [H_BIT-1:0]hcnt_in,
    input wire [V_BIT-1:0]vcnt_in,
    output vgaPkg::vga_t vga
);
    logic [6:0]hcnt;
    logic [5:0]vcnt;
    logic [2:0]img[5119:0];

    initial $readmemb(NAME, img);

    assign hcnt = hcnt_in[9:3];
    assign vcnt = vcnt_in[8:3];

    always_ff @(posedge clk)
        if (!rst) begin
            vga.hsync <= '0;
            vga.vsync <= '0;
            vga.red <= '0;
            vga.green <= '0;
            vga.blue <= '0;
        end else begin
            vga.hsync <= hsync_in;
            vga.vsync <= vsync_in;
            {vga.red, vga.green, vga.blue} <=
              valid_in ? img[{hcnt, vcnt}] : '0;
        end

endmodule

`default_nettype wire
