/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module vga_tb;
    logic clk, rst;

    initial begin
        clk <= 1'b0;
        forever #20ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        repeat(2)
            @(posedge clk);
        rst <= 1'b1;
    end

    vga dut (
        .clk(clk),
        .rst(rst),
        .hsync(),
        .vsync(),
        .valid(),
        .hcnt(),
        .vcnt());

endmodule

`default_nettype wire
