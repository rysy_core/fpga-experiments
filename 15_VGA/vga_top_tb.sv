/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps/10ps

module vga_top_tb;
    logic clk, rst;

    initial begin
        clk <= 1'b0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        repeat(2)
            @(posedge clk);
        rst <= 1'b1;
    end

    vga_top dut (
        .clk(clk),
        .rst(rst),
        .hsync(),
        .vsync(),
        .red(),
        .green(),
        .blue());

endmodule

`default_nettype wire
