/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

package vgaPkg;

	typedef struct packed {
        logic red;
        logic green;
        logic blue;
        logic hsync;
        logic vsync;
	} vga_t;

endpackage : vgaPkg

`default_nettype wire
