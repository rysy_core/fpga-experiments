/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module vga #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V),
    parameter H_VALID_MIN = 0,
    parameter H_VALID_MAX = 639,
    parameter V_VALID_MIN = 0,
    parameter V_VALID_MAX = 479,
    parameter H_SYNC_MIN = 656,
    parameter H_SYNC_MAX = 751,
    parameter V_SYNC_MIN = 490,
    parameter V_SYNC_MAX = 491
) (
    input wire clk,
    input wire rst,
    output logic hsync,
    output logic vsync,
    output logic valid,
    output logic [H_BIT-1:0]hcnt,
    output logic [V_BIT-1:0]vcnt
);
    logic ov;
    logic [H_BIT-1:0]hcnt_r;
    logic [V_BIT-1:0]vcnt_r;

    counter #(.N(H)) counter_H (
        .clk(clk),
        .rst(rst),
        .ce(1'b1),
        .q(hcnt_r),
        .ov(ov));

    counter #(.N(V)) counter_V (
        .clk(clk),
        .rst(rst),
        .ce(ov),
        .q(vcnt_r),
        .ov());

    always_ff @(posedge clk)
        if (!rst) begin
            hcnt <= '0;
            vcnt <= '0;
            hsync <= '0;
            vsync <= '0;
            valid <= '0;
        end else begin
            hcnt <= hcnt_r;
            vcnt <= vcnt_r;
            hsync <= !(hcnt_r >= H_SYNC_MIN && hcnt_r <= H_SYNC_MAX);
            vsync <= !(vcnt_r >= V_SYNC_MIN && vcnt_r <= V_SYNC_MAX);
            valid <= hcnt_r >= H_VALID_MIN && hcnt_r <= H_VALID_MAX
                && vcnt_r >= V_VALID_MIN && vcnt_r <= V_VALID_MAX;
        end
endmodule

`default_nettype wire
