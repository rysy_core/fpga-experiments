/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps/10ps

module vga_top_tb;
    logic clk, rst, s;

    initial begin
        clk <= 1'b0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        repeat(2)
            @(posedge clk);
        rst <= 1'b1;
    end

    assign s = 1'b0;

    vga_top1 dut (
        .clk(clk),
        .rst(rst),
        .s(s),
        .hsync(),
        .vsync(),
        .red(),
        .green(),
        .blue());

endmodule

`default_nettype wire
