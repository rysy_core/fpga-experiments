/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps/10ps

module rectangle_top_tb;
    parameter ENC_TIME = 35000;
    logic clk, rst;
    logic ea, eb;
    vgaPkg::vga_t vga;

    initial begin
        clk <= 1'b0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        repeat(6) @(posedge clk);
        rst <= 1'b1;
    end

    initial begin
        {ea, eb} = '0;
        for (int i = 0; i < 15; i++) begin
            eb = 1'b1;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
            ea = 1'b1;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
            eb = 1'b0;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
            ea = 1'b0;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
        end
        for (int i = 0; i < 7; i++) begin
            ea = 1'b1;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
            eb = 1'b1;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
            ea = 1'b0;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
            eb = 1'b0;
            repeat(ENC_TIME) @(posedge dut.clk_vga);
        end
        @(negedge vga.vsync);
        #5us;
        $stop;
    end

    rectangle_top dut (
        .clk(clk),
        .rst(rst),
        .a(ea),
        .b(eb),
        .vga(vga));

    vga_monitor #(
        .NAME("rectangle")
    ) monitor (
        .clk(dut.clk_vga),
        .rst(rst),
        .c({vga.red, vga.green, vga.blue}),
        .hsync(vga.hsync),
        .vsync(vga.vsync));

endmodule

`default_nettype wire
