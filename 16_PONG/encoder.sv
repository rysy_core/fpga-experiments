/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module encoder #(
	parameter MAX = 640,
    parameter MAX_N = $clog2(MAX),
	parameter DEBONUCE_N = 14
) (
	input wire clk,
	input wire rst,
	input wire a,
	input wire b,
	output logic [MAX_N-1:0]x
);
	logic a_r, b_r;
	logic [MAX_N-1:0]cnt;
	logic a_d, a_e, b_d;

	always_ff @(posedge clk or negedge rst)
		if (!rst) begin
			a_r <= '0;
			b_r <= '0;
		end else begin
			a_r <= a;
			b_r <= b;
		end

	debounce #(.N(DEBONUCE_N)) db_a (
		.clk(clk),
		.rst(rst),
		.d(a_r),
		.q(a_d)
	);

	debounce #(.N(DEBONUCE_N)) db_b (
		.clk(clk),
		.rst(rst),
		.d(b_r),
		.q(b_d)
	);

	edge_detector ed_a (
		.clk(clk),
		.rst(rst),
		.d(a_d),
		.q(a_e)
	);

	always_ff @(posedge clk)
        if (!rst)
            cnt <= '0;
        else if(a_e)
            if(b_d && cnt < MAX-1)
                cnt <= cnt + 1'd1;
            else if (!b_d && cnt > 0)
                cnt <= cnt - 1'd1;

    always_ff @(posedge clk)
        if (!rst)
            x <= '0;
        else
            x <= cnt;

endmodule

`default_nettype wire
