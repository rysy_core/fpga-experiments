#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ball.sv
vlog ball_tb.sv

vsim work.ball_tb

add wave -position end sim:/ball_tb/clk
add wave -position end sim:/ball_tb/rst
add wave -position end sim:/ball_tb/game
add wave -position end sim:/ball_tb/ce
add wave -position end sim:/ball_tb/dut/reflection
add wave -position end -unsigned sim:/ball_tb/dut/by_init;
add wave -position end -unsigned sim:/ball_tb/dut/ball_x
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 640 sim:/ball_tb/dut/ball_x
add wave -position end -unsigned sim:/ball_tb/dut/ball_y
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 480 sim:/ball_tb/dut/ball_y

run 100us
wave zoom full
