/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module pong_top #(
    parameter H = 800,
    parameter V = 525,
    parameter D_H = 640,
    parameter H_V = 480,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire a,
    input wire b,
    input button,
    output vgaPkg::vga_t vga
);
    parameter RECTANGLE_H = 20;
    parameter RECTANGLE_V = 80;
    parameter BALL = 20;

    logic [H_BIT-1:0]hcnt_vga_l;
    logic [V_BIT-1:0]vcnt_vga_l;

    logic [H_BIT-1:0]ball_x;
    logic [V_BIT-1:0]ball_y;
    logic [V_BIT-1:0]opp_y;
    logic [V_BIT-1:0]p_y;
    logic [V_BIT-1:0]player_y;
    logic clk_vga, valid_vga, hsync_vga, ce, button_d;
    logic [1:0] vsync_vga;
    pongPkg::state_t state;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

	debounce #(.N(20)) db_button (
		.clk(clk_vga),
		.rst(rst),
		.d(!button),
		.q(button_d));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga[0]),
        .valid(valid_vga),
        .hcnt(hcnt_vga_l),
        .vcnt(vcnt_vga_l));

    always_ff @(posedge clk_vga)
        vsync_vga[1] <= vsync_vga[0];

    assign ce = (!vsync_vga[0]) & vsync_vga[1];

	encoder #(
		.MAX((479-RECTANGLE_V)/32+1)
	) enc (
		.clk(clk_vga),
		.rst(rst),
		.a(a),
		.b(b),
		.x(p_y));

    assign player_y = {p_y, 5'd0};

    pong #(
        .RECTANGLE_H(RECTANGLE_H),
        .RECTANGLE_V(RECTANGLE_V),
        .BALL(BALL)
    ) pong_inst (
        .clk(clk_vga),
        .rst(rst),
        .ce(ce),
        .button(button_d),
        .encoder(player_y),
        .ball_x(ball_x),
        .ball_y(ball_y),
        .opp_y(opp_y),
        .state(state));

    pong_disp #(
        .RECTANGLE_H(RECTANGLE_H),
        .RECTANGLE_V(RECTANGLE_V),
        .BALL(BALL)
    ) pong_disp_inst (
        .clk(clk_vga),
        .rst(rst),
        .ball_x(ball_x),
        .ball_y(ball_y),
        .opp_y(opp_y),
        .player_y(player_y),
        .state(state),
        .hsync_vga(hsync_vga),
        .vsync_vga(vsync_vga[0]),
        .valid_vga(valid_vga),
        .hcnt_vga(hcnt_vga_l),
        .vcnt_vga(vcnt_vga_l),
        .vga(vga));

endmodule

`default_nettype wire
