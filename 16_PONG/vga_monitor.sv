/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module vga_monitor #(
    parameter NAME = "frame"
) (
    input wire clk,
    input wire rst,
    input wire [2:0]c,
    input wire hsync,
    input wire vsync
);
    parameter V = 480;
    parameter V_offset = 33;
    parameter H = 640;
    parameter H_offset = 48;
    int frame;
    logic [2:0][7:0]ct[][];
    int x, y;
    logic last_vsync, last_hsync;

    initial begin
        ct = new[V];
        for (int iy = 0; iy < V; iy++)
            ct[iy] = new[H];
    end

    always @(posedge clk) begin
        if (!rst) begin
            x = 0;
            y = 0;
            frame = 0;
            last_vsync = 1'd0;
            last_hsync = 1'd0;
        end else begin            
            if (vsync == 1'd0) begin
                if (last_vsync == 1'd1) begin
                    bmpPkg::save($sformatf("%s_%02d.bmp", NAME, frame), H, V, ct);
                    frame = frame + 1;
                end
                x = 0;
                y = 0;
            end else if (hsync == 1'd0) begin
                x = 0;
                if (last_hsync == 1'd1)
                    y = y + 1;
            end else begin
                x = x + 1;
            end
            last_vsync = vsync;
            last_hsync = hsync;
            if ((x >= H_offset && x < (H + H_offset)) && (y >= V_offset && y < (V + V_offset))) begin
                for (int cid = 0; cid < 3; cid++)
                    ct[V-1-(y-V_offset)][x-H_offset][cid] = c[2-cid] ? 8'd255 : 8'd0;
            end
        end
    end

endmodule
 
`default_nettype wire
 