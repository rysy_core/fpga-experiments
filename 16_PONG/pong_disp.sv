/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module pong_disp 
    import pongPkg::*;
#(
    parameter H = 800,
    parameter V = 525,
    parameter RECTANGLE_H = 20,
    parameter RECTANGLE_V = 80,
    parameter BALL = 20,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire [H_BIT-1:0]ball_x,
    input wire [V_BIT-1:0]ball_y,
    input wire [V_BIT-1:0]opp_y,
    input wire [V_BIT-1:0]player_y,
    input wire hsync_vga,
    input wire vsync_vga,
    input wire valid_vga,
    input wire [H_BIT-1:0]hcnt_vga,
    input wire [V_BIT-1:0]vcnt_vga,
    input state_t state,
    output vgaPkg::vga_t vga
);
   vgaPkg::vga_t vga_p, vga_i, vga_v, vga_d, vga_o, vga_b, vga_pl;

    rectangle #(
        .V(BALL), .H(BALL),
        .logX(H_BIT), .logY(V_BIT),
        .color(3'b111)
    ) rect_ball (
        .clk(clk),
        .px(ball_x),
        .py(ball_y),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(vga_b));

    rectangle #(
        .V(RECTANGLE_V), .H(RECTANGLE_H),
        .logX(H_BIT), .logY(V_BIT),
        .color(3'b100)
    ) rect_opp (
        .clk(clk),
        .px(639-RECTANGLE_H),
        .py(opp_y),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(vga_o));

    rectangle #(
        .V(RECTANGLE_V), .H(RECTANGLE_H),
        .logX(H_BIT), .logY(V_BIT),
        .color(3'b001)
    ) rect_player (
        .clk(clk),
        .px(0),
        .py(player_y),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(vga_pl));

    assign vga_p.hsync = vga_b.hsync;
    assign vga_p.vsync = vga_b.vsync;
    assign vga_p.red = vga_b.red | vga_o.red | vga_pl.red;
    assign vga_p.green = vga_b.green | vga_o.green | vga_pl.green;
    assign vga_p.blue = vga_b.blue | vga_o.blue | vga_pl.blue;

    from_mem #(
        .NAME("images/init.mem")
    ) image_init (
        .clk(clk),
        .rst(rst),
        .hsync_in(hsync_vga),
        .vsync_in(vsync_vga),
        .valid_in(valid_vga),
        .hcnt_in(hcnt_vga),
        .vcnt_in(vcnt_vga),
        .vga(vga_i));

    from_mem #(
        .NAME("images/victory.mem")
    ) image_victory (
        .clk(clk),
        .rst(rst),
        .hsync_in(hsync_vga),
        .vsync_in(vsync_vga),
        .valid_in(valid_vga),
        .hcnt_in(hcnt_vga),
        .vcnt_in(vcnt_vga),
        .vga(vga_v));

    from_mem #(
        .NAME("images/defeat.mem")
    ) image_defeat (
        .clk(clk),
        .rst(rst),
        .hsync_in(hsync_vga),
        .vsync_in(vsync_vga),
        .valid_in(valid_vga),
        .hcnt_in(hcnt_vga),
        .vcnt_in(vcnt_vga),
        .vga(vga_d));

    always_ff @(posedge clk)
        case (state)
            INIT: vga <= vga_i;
            PONG: vga <= vga_p;
            VICTORY: vga <= vga_v;
            DEFEAT: vga <= vga_d;
        endcase

endmodule

`default_nettype wire
