/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module opponent_top #(
    parameter H = 800,
    parameter V = 525,
    parameter D_H = 640,
    parameter H_V = 480,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire a,
    input wire b,
    input button,
    output vgaPkg::vga_t vga
);
    parameter RECTANGLE_H = 20;
    parameter RECTANGLE_V = 80;
    parameter BALL = 20;

    logic [H_BIT-1:0]hcnt_vga;
    logic [V_BIT-1:0]vcnt_vga;
    logic [H_BIT-1:0]ball_x;
    logic [V_BIT-1:0]ball_y;
    logic [V_BIT-1:0]opp_y;
    logic clk_vga, valid_vga, vsync_vga_r;
    logic hsync_vga, vsync_vga, ce;
    vgaPkg::vga_t b_vga, o_vga;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

    always_ff @(posedge clk_vga)
        vsync_vga_r <= vsync_vga;

    assign ce = vsync_vga & !vsync_vga_r;

    ball #(
        .BALL_H(BALL),
        .BALL_V(BALL),
        .BALL_X_MIN(RECTANGLE_H),
        .BALL_X_MAX(D_H-1-RECTANGLE_H)
    ) ball_inst (
        .clk(clk_vga),
        .rst(rst),
        .game(button),
        .ce(ce),
        .ball_x(ball_x),
        .ball_y(ball_y));

    opponent #(
        .HIGH(RECTANGLE_V),
        .BALL_HIGH(BALL),
        .VELOCITY(5)
    ) opponent_inst (
        .clk(clk_vga),
        .rst(rst),
        .ce(ce),
        .ball_y(ball_y),
        .opponent_y(opp_y));

    rectangle #(
        .V(BALL), .H(BALL),
        .logX(H_BIT), .logY(V_BIT),
        .color(3'b111)
    ) rect_ball (
        .clk(clk_vga),
        .px(ball_x),
        .py(ball_y),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(b_vga));

    rectangle #(
        .V(RECTANGLE_V), .H(RECTANGLE_H),
        .logX(H_BIT), .logY(V_BIT),
        .color(3'b100)
    ) rect_opp (
        .clk(clk_vga),
        .px(639-RECTANGLE_H),
        .py(opp_y),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(o_vga));

    always_ff @(posedge clk_vga) begin
        vga.hsync <= b_vga.hsync;
        vga.vsync <= b_vga.vsync;
        vga.red <= b_vga.red | o_vga.red;
        vga.green <= b_vga.green | o_vga.green;
        vga.blue <= b_vga.blue | o_vga.blue;
    end
endmodule

`default_nettype wire
