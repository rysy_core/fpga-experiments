/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps/10ps

module vga_top_tb;
    logic clk, rst;
    logic r, g, b;
    logic hsync, vsync;

    initial begin
        clk <= 1'b0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        repeat(8)
            @(posedge clk);
        rst <= 1'b1;
    end

    vga_top dut (
        .clk(clk),
        .rst(rst),
        .hsync(hsync),
        .vsync(vsync),
        .red(r),
        .green(g),
        .blue(b));

    vga_monitor monitor (
        .clk(dut.clk_vga),
        .rst(rst),
        .c({r, g, b}),
        .hsync(hsync),
        .vsync(vsync));

endmodule

`default_nettype wire
