/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

package pongPkg;

    typedef enum logic [1:0] {
        INIT,
        PONG,
        VICTORY,
        DEFEAT
    } state_t;

endpackage : pongPkg

`default_nettype wire
