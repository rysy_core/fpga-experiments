#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../15_VGA/vga_pkg.sv
vlog rectangle.sv
vlog rectangle_tb.sv

vsim work.rectangle_tb

add wave -position end sim:/rectangle_tb/clk
add wave -position end sim:/rectangle_tb/px
add wave -position end sim:/rectangle_tb/py
add wave -position end sim:/rectangle_tb/x
add wave -position end sim:/rectangle_tb/y
add wave -position end sim:/rectangle_tb/valid
add wave -position end sim:/rectangle_tb/vsync
add wave -position end sim:/rectangle_tb/dut/px_t
add wave -position end sim:/rectangle_tb/dut/py_t
add wave -position end sim:/rectangle_tb/dut/vga

run -all
wave zoom full
