#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog bmp_pkg.sv
vlog bmp_save.sv

vsim work.bmp_save

run -all
