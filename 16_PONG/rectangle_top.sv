/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module rectangle_top #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire a,
    input wire b,
    output vgaPkg::vga_t vga
);
    parameter RECTANGLE_H = 20;
    parameter RECTANGLE_V = 80;
    logic [H_BIT-1:0]hcnt_vga;
    logic [V_BIT-1:0]vcnt_vga;
    logic [V_BIT-1:0]py;
    logic clk_vga, valid_vga, hsync_vga, vsync_vga;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

	encoder #(
		.MAX((480-RECTANGLE_V)/8+1)
	) enc (
		.clk(clk_vga),
		.rst(rst),
		.a(a),
		.b(b),
		.x(py));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

    rectangle #(
        .V(RECTANGLE_V), .H(RECTANGLE_H),
        .logX(H_BIT), .logY(V_BIT)
    ) rect (
        .clk(clk_vga),
        .px(10),
        .py({py, 3'd0}),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(vga));

endmodule

`default_nettype wire
