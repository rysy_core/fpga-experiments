/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

package bmpPkg;
    /*
        width % 4 must be 0
    */

    typedef struct packed {
        logic [15:0]mn;
        logic [31:0]size;
        logic [31:0]res;
        logic [31:0]offset;
    } bmp_header;
    
    typedef struct packed {
        logic [31:0] biSize;
        logic [31:0] biWidth;
        logic [31:0] biHeight;
        logic [15:0] biPlanes;
        logic [15:0] biBitCount;
        logic [31:0] biCompression;
        logic [31:0] biSizeImage;
        logic [31:0] biXPelsPerMeter;
        logic [31:0] biYPelsPerMeter;
        logic [31:0] biClrUsed;
        logic [ 7:0] biClrImportant;
        logic [ 7:0] biClrRotation;
        logic [15:0] biReserved;
    } bmp_info_header;

    task show(string fname);
        int a1, f, i;
        logic [7:0]b;
        bmp_header bh;
        bmp_info_header bih;
        logic [2:0][7:0]c;
        a1 = $fopen(fname, "rb");
    
        f = $fread(bh, a1);
        $display("BMP header:");
        $display("Magic number: 0x%x", bh.mn);
        $display("size: %d", {<<8{bh.size}});
        $display("res: %d", {<<8{bh.res}});
        $display("offset: %d", {<<8{bh.offset}});
    
        f = $fread(bih, a1);
        $display("BMP info header:");
        $display("biSize: %d", {<<8{bih.biSize}});
        $display("biWidth: %d", {<<8{bih.biWidth}});
        $display("biHeight: %d", {<<8{bih.biHeight}});
        $display("biPlanes: %d", {<<8{bih.biPlanes}});
        $display("biBitCount: %d", {<<8{bih.biBitCount}});
        $display("biCompression: %d", {<<8{bih.biCompression}});
        $display("biSizeImage: %d", {<<8{bih.biSizeImage}});
        $display("biXPelsPerMeter: %d", {<<8{bih.biXPelsPerMeter}});
        $display("biYPelsPerMeter: %d", {<<8{bih.biYPelsPerMeter}});
        $display("biClrUsed: %d", {<<8{bih.biClrUsed}});
        $display("biClrImportant: %d", {<<8{bih.biClrImportant}});
        $display("biClrRotation: %d", {<<8{bih.biClrRotation}});
        $display("biReserved: %d", {<<8{bih.biReserved}});
    
        for (int y=0; y<{<<8{bih.biHeight}}; y++) begin
            for (int x=0; x<{<<8{bih.biWidth}}; x++) begin
                f = $fread(c, a1);
                $display("x: %d, y: %d, r: %d, g: %d, b: %d", x, y, c[0], c[1], c[2]);
            end
        end    
    
        $fclose(a1);
    endtask

    task automatic save(string fname, int width, int height, ref logic [2:0][7:0]c[][]);
        int f;
        bmp_header bh;
        bmp_info_header bih;
        logic [31:0] size;
        logic [7:0] bh_b[13:0];
        logic [7:0] bih_b[39:0];

        size = $bits(bh)/8 + $bits(bih)/8 + width*height*3;

        bh.mn = 16'h424d;
        bh.size = {<<8{size}};
        bh.res = '0;
        bh.offset = {<<8{32'd54}};

        {>>{bh_b}} = bh;

        bih.biSize = {<<8{16'd40}};
        bih.biWidth = {<<8{width}};
        bih.biHeight = {<<8{height}};
        bih.biPlanes = {<<8{16'd1}};
        bih.biBitCount = {<<8{16'd24}};
        bih.biCompression = '0;
        bih.biSizeImage = {<<8{width*height*3}};
        bih.biXPelsPerMeter = '0;
        bih.biYPelsPerMeter = '0;
        bih.biClrUsed = '0;
        bih.biClrImportant = '0;
        bih.biClrRotation = '0;
        bih.biReserved = '0;

        {>>{bih_b}} = bih;

        f = $fopen(fname, "wb");
        for (int i=0; i < 14; i++)
            $fwrite(f, "%c", bh_b[13-i]);
        for (int i=0; i < 40; i++)
            $fwrite(f, "%c", bih_b[39-i]);

        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                for (int z = 0; z < 3; z++)
            $fwrite(f, "%c", c[y][x][2-z]);

        $fclose(f);
    endtask

endpackage : bmpPkg

`default_nettype wire
