/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module opponent #(
	parameter V = 480,
    parameter H = 640,
    parameter LOG_Y = $clog2(V),
    parameter HIGH = 100,
    parameter BALL_HIGH = 20,
    parameter VELOCITY = 2
) (
	input wire clk,
    input wire rst,
    input wire ce,
    input wire [LOG_Y-1:0] ball_y,
    output logic [LOG_Y-1:0] opponent_y
);
    parameter Y_MIN = 0;
    parameter Y_MAX = V-1;

    logic signed [LOG_Y:0] oy_tmp;
    logic signed [LOG_Y:0] oy;
    logic signed [LOG_Y:0] by;
    logic ce1;

    assign by = {1'b0, ball_y};

    always_ff @(posedge clk)
        if (!rst)
            ce1 <= '0;
        else
            ce1 <= ce;

    always_ff @(posedge clk)
        if (!rst) begin
            opponent_y <= (V-HIGH)/2;
            oy <= (V-HIGH)/2;
        end else begin
            if (ce) begin
                oy_tmp <= oy + (((by - HIGH/2 + BALL_HIGH/2) > oy) ? VELOCITY : -VELOCITY);
            end

            if (ce1) begin
                if (oy_tmp < Y_MIN)
                    oy <= Y_MIN;
                else if (oy_tmp > Y_MAX - HIGH)
                    oy <= Y_MAX-HIGH;
                else
                    oy <= oy_tmp;
            end
            opponent_y <= oy;
        end

endmodule

`default_nettype wire
