/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module rectangle #(
	parameter V = 20,
    parameter H = 80,
    parameter color = 3'b111,
    parameter logX = 9,
    parameter logY = 8
) (
	input wire clk,
    input wire [logX-1:0]px,
    input wire [logY-1:0]py,
    input wire [logX-1:0]x,
    input wire [logY-1:0]y,
    input wire valid,
    input wire vsync,
    input wire hsync,
    output vgaPkg::vga_t vga
);
    logic x_in, y_in;
    logic [logX-1:0]px_t;
    logic [logY-1:0]py_t;

    always_ff @(posedge clk)
        if (vsync == 1'b0)
            {px_t, py_t} <= {px, py};

    assign x_in = x >= px_t && x < (px_t+H);
    assign y_in = y >= py_t && y < (py_t+V);

    always_ff @(posedge clk)
        if (x_in && y_in && valid)
            {vga.red, vga.green, vga.blue} <= color;
        else
            {vga.red, vga.green, vga.blue} <= '0;

    always_ff @(posedge clk) begin
        vga.hsync <= hsync;
        vga.vsync <= vsync;
    end

endmodule

`default_nettype wire
