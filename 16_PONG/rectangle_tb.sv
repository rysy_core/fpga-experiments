/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module rectangle_tb;
    parameter logX = 4;
    parameter logY = 4;
    int frame;
	logic clk, valid, hsync, vsync;
    logic [logX-1:0]x;
    logic [logY-1:0]y;
    logic [logX-1:0]px;
    logic [logY-1:0]py;

	initial begin
		clk <= '0;
		forever #5ns clk = ~clk;
	end

    initial begin
        for (frame = 0; frame < 4; frame++)
            for (y = 0; y < 10; y++)
                for (x = 0; x < 15; x++)
                    @(posedge clk);
        $stop;
    end

    assign vsync = !(y == 9 && x == 14);
    assign valid = (y < 8 && x < 12);

    always_ff @(posedge clk)
        if (x == 5 && y == 5) begin
            px = 2*frame;
            py = 2*frame;
        end

    rectangle #(
        .V(2), .H(3),
        .logX(4), .logY(4)
    ) dut (
        .clk(clk),
        .px(px),
        .py(py),
        .x(x),
        .y(y),
        .valid(valid),
        .hsync(1'b0),
        .vsync(vsync),
        .vga());

endmodule

`default_nettype wire
