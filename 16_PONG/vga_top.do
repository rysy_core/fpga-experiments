#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

set QUARTUS_DIR "C:/intelFPGA_lite/18.1/quartus/eda/sim_lib/"

vlib work

vlog $QUARTUS_DIR/altera_mf.v

vlog ../02_counter/counter.sv
vlog ../15_VGA/vga.sv
vlog ../15_VGA/squares.sv
vlog ../15_VGA/pll.v
vlog bmp_pkg.sv
vlog vga_monitor.sv
vlog ../15_VGA/vga_top.sv
vlog vga_top_tb.sv

vsim work.vga_top_tb

add wave -position end sim:/vga_top_tb/clk
add wave -position end sim:/vga_top_tb/dut/clk_vga
add wave -position end sim:/vga_top_tb/rst

add wave -divider out

add wave -position end sim:/vga_top_tb/dut/hsync
add wave -position end sim:/vga_top_tb/dut/vsync
add wave -position end sim:/vga_top_tb/dut/red
add wave -position end sim:/vga_top_tb/dut/green
add wave -position end sim:/vga_top_tb/dut/blue

add wave -divider vga_monitor

add wave -position end sim:/vga_top_tb/monitor/x
add wave -position end sim:/vga_top_tb/monitor/y

run 50ms
wave zoom full
