/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module opponent_tb;
    parameter V = 480;
    parameter LOG_V = $clog2(V);

	logic clk, rst, ce;
    logic [LOG_V-1:0] ball_y; 

	initial begin
		clk <= '0;
		forever #5ns clk = ~clk;
	end

    initial begin
        rst <= '0;
        repeat(2)
            @(posedge clk);
        rst <= '1;
    end

    initial begin
        ce <= 1'b0;
        @(posedge clk);
        forever begin
            ce <= !ce;
            @(posedge clk);
        end
    end

    initial begin
        ball_y <= 0;
        #100 ball_y <= V-1;
        #100 ball_y <= V/2;
        #100 ball_y <= 2*V/3;
        #100 $stop;
    end

    opponent dut (
        .clk(clk),
        .rst(rst),
        .ce(ce),
        .ball_y(ball_y),
        .opponent_y());

endmodule

`default_nettype wire
