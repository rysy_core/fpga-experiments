/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module ball_top #(
    parameter H = 800,
    parameter V = 525,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire a,
    input wire b,
    input button,
    output vgaPkg::vga_t vga
);
    parameter RECTANGLE_H = 20;
    parameter RECTANGLE_V = 80;
    logic [H_BIT-1:0]hcnt_vga;
    logic [V_BIT-1:0]vcnt_vga;
    logic [H_BIT-1:0]ball_x;
    logic [V_BIT-1:0]ball_y;
    logic clk_vga, valid_vga, hsync_vga, vsync_vga, vsync_vga_r;

    pll pll_inst (
        .inclk0(clk),
        .c0(clk_vga));

    vga #(.H(H), .V(V)) vga_inst (
        .clk(clk_vga),
        .rst(rst),
        .hsync(hsync_vga),
        .vsync(vsync_vga),
        .valid(valid_vga),
        .hcnt(hcnt_vga),
        .vcnt(vcnt_vga));

    always_ff @(posedge clk_vga)
        vsync_vga_r <= vsync_vga;

    ball #(.BALL_H(20), .BALL_V(20)) ball_inst (
        .clk(clk_vga),
        .rst(rst),
        .game(button),
        .ce(vsync_vga & !vsync_vga_r),
        .ball_x(ball_x),
        .ball_y(ball_y));

    rectangle #(
        .V(20), .H(20),
        .logX(H_BIT), .logY(V_BIT)
    ) rect (
        .clk(clk_vga),
        .px(ball_x),
        .py(ball_y),
        .x(hcnt_vga),
        .y(vcnt_vga),
        .valid(valid_vga),
        .vsync(vsync_vga),
        .hsync(hsync_vga),
        .vga(vga));

endmodule

`default_nettype wire
