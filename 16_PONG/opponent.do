#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog opponent.sv
vlog opponent_tb.sv

vsim work.opponent_tb

add wave -position end sim:/opponent_tb/clk
add wave -position end sim:/opponent_tb/rst
add wave -position end sim:/opponent_tb/ce
add wave -position end -unsigned sim:/opponent_tb/ball_y
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 480 sim:/opponent_tb/ball_y
add wave -position end -unsigned sim:/opponent_tb/dut/opponent_y
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 480 sim:/opponent_tb/dut/opponent_y
add wave -position end -unsigned sim:/opponent_tb/dut/oy_tmp
add wave -position end -unsigned sim:/opponent_tb/dut/oy

run -all
wave zoom full
