/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module ball #(
	parameter V = 480,
    parameter H = 640,
    parameter BALL_X_MIN = 0,
    parameter BALL_X_MAX = H-1,
    parameter BALL_Y_MIN = 0,
    parameter BALL_Y_MAX = V-1,
    parameter BALL_V = 20,
    parameter BALL_H = 20,
    parameter V_X = 40,
    parameter V_Y = 12,
    parameter LOG_X = $clog2(H),
    parameter LOG_Y = $clog2(V)
) (
	input wire clk,
    input wire rst,
    input wire game,
    input wire ce,
    output logic reflection,
    output logic [LOG_X-1:0] ball_x,
    output logic [LOG_Y-1:0] ball_y
);
    logic signed [4:0]ball_vx;
    logic signed [4:0]ball_vy;
    logic signed [LOG_X:0] bx; 
    logic signed [LOG_Y:0] by; 
    logic signed [LOG_X:0] bx_tmp; 
    logic signed [LOG_Y:0] by_tmp;
    logic signed [LOG_Y:0] by_init; 
    logic ce1;
    logic reflection_tmp;

    always_ff @(posedge clk)
        if (!rst)
            ce1 <= '0;
        else
            ce1 <= ce;

    always_ff @(posedge clk)
        if (!rst)
            by_init <= BALL_Y_MIN;
        else begin
            if (by_init < BALL_Y_MAX-BALL_V)
                by_init <= by_init + 1'd1;
            else
                by_init <= BALL_Y_MIN;
        end

    always_ff @(posedge clk)
        if (!rst) begin
            bx <= H/2;
            by <= V/2;
            bx_tmp <= H/2;
            by_tmp <= V/2;
            ball_x <= H/2;
            ball_y <= V/2;
            ball_vx <= V_X;
            ball_vy <= V_Y;
            reflection_tmp <= '0;
        end else if (!game) begin
            bx <= H/2;
            by <= by_init;
            bx_tmp <= H/2;
            by_tmp <= by_init;
            ball_x <= H/2;
            ball_y <= by_init;
            ball_vx <= V_X;
            ball_vy <= V_Y;
            reflection_tmp <= '0;
        end else begin
            if (ce) begin
                bx_tmp <= bx + ball_vx;
                by_tmp <= by + ball_vy;
            end
            if (ce1) begin
                if (bx_tmp < BALL_X_MIN) begin
                    bx <= BALL_X_MIN;
                    ball_vx <= -ball_vx;
                    reflection_tmp <= '1;
                end else if (bx_tmp > BALL_X_MAX - BALL_H) begin
                    bx <= BALL_X_MAX - BALL_H;
                    ball_vx <= -ball_vx;
                    reflection_tmp <= '1;
                end else begin
                    bx <= bx_tmp;
                    reflection_tmp <= '0;
                end

                if (by_tmp < BALL_Y_MIN) begin
                    by <= BALL_Y_MIN;
                    ball_vy <= -ball_vy;
                end else if (by_tmp > BALL_Y_MAX - BALL_V) begin
                    by <= BALL_Y_MAX - BALL_V;
                    ball_vy <= -ball_vy;
                end else begin
                    by <= by_tmp;
                end
            end

            ball_x <= bx;
            ball_y <= by;
            reflection <= reflection_tmp;
        end

endmodule

`default_nettype wire
