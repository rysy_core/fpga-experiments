/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module bmp_open;

    initial
        bmpPkg::show("test.bmp");

endmodule
 
`default_nettype wire
