/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module ball_tb;
	logic clk, rst, ce, game;

	initial begin
		clk <= '0;
		forever #5ns clk = ~clk;
	end

    initial begin
        rst <= '0;
        repeat(2)
            @(posedge clk);
        rst <= '1;
    end

    initial begin
        ce <= 1'b0;
        @(posedge clk);
        forever begin
            ce <= !ce;
            @(posedge clk);
        end
    end

    initial begin
        game <= 1'b0;
        repeat(10) @(posedge clk);
        game <= 1'b1;
        repeat(400) @(posedge clk);
        game <= 1'b0;
        repeat(40) @(posedge clk);
        game <= 1'b1;
        repeat(800) @(posedge clk);
        $stop;
    end

    ball dut (
        .clk(clk),
        .rst(rst),
        .game(game),
        .ce(ce),
        .reflection(),
        .ball_x(),
        .ball_y());

endmodule

`default_nettype wire
