/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module pong
    import pongPkg::*;
#(
    parameter H = 800,
    parameter V = 525,
    parameter D_H = 640,
    parameter H_V = 480,
    parameter RECTANGLE_H = 20,
    parameter RECTANGLE_V = 80,
    parameter BALL = 20,
    parameter BALL_V_X = 7,
    parameter BALL_V_Y = 5,
    parameter OPP_V = 4,
    parameter H_BIT = $clog2(H),
    parameter V_BIT = $clog2(V)
) (
    input wire clk,
    input wire rst,
    input wire ce,
    input wire button,
    input wire [V_BIT-1:0]encoder,
    output logic [H_BIT-1:0]ball_x,
    output logic [V_BIT-1:0]ball_y,
    output logic [V_BIT-1:0]opp_y,
    output state_t state
);

    wire new_game, reflection;

    ball #(
        .BALL_H(BALL),
        .BALL_V(BALL),
        .BALL_X_MIN(RECTANGLE_H),
        .BALL_X_MAX(D_H-1-RECTANGLE_H),
        .V_X(BALL_V_X),
        .V_Y(BALL_V_Y)
    ) ball_inst (
        .clk(clk),
        .rst(rst),
        .game(!new_game),
        .ce(ce),
        .reflection(reflection),
        .ball_x(ball_x),
        .ball_y(ball_y));

    opponent #(
        .HIGH(RECTANGLE_V),
        .BALL_HIGH(BALL),
        .VELOCITY(OPP_V)
    ) opponent_inst (
        .clk(clk),
        .rst(!new_game),
        .ce(ce),
        .ball_y(ball_y),
        .opponent_y(opp_y));

    function automatic check_ball([V_BIT-1:0]pos);
        
        logic signed [V_BIT:0]pos_ball = $signed({1'b0, pos}) - $signed(BALL);
        logic signed [V_BIT:0]pos_rec = pos + RECTANGLE_V;
        logic signed [V_BIT:0]ball = ball_y;
        
        check_ball = (ball < pos_ball || ball > pos_rec);
    endfunction

//    function automatic check_ball([V_BIT-1:0]pos, [V_BIT-1:0]ball);
//        check_ball = (ball < pos || ball > pos+RECTANGLE_V);
//    endfunction

    always_ff @(posedge clk)
        if (!rst)
            state <= INIT;
        else if (ce) begin
            case (state)
                INIT: state <= button ? PONG : INIT;
                PONG:
                    if (reflection)
                        if (ball_x < 320)
                            state <= check_ball(encoder) ? DEFEAT : PONG;
                        else
                            state <= check_ball(opp_y) ? VICTORY : PONG;
                VICTORY: state <= button ? PONG : VICTORY;
                DEFEAT: state <= button ? PONG : DEFEAT;
            endcase
        end

    assign new_game = state != PONG;

endmodule

`default_nettype wire
