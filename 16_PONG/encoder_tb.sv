/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module encoder_tb;
    localparam MAX = 10;
    localparam DEB = 2;
	logic clk, rst, a, b;

	initial begin
		clk <= '0;
		forever #5ns clk = ~clk;
	end

	initial begin
		rst <= 1'b0;
		#15ns rst <= 1'b1;
	end

	initial begin
		a <= 1'b0;
		b <= 1'b1;
		repeat (15) begin
			#50ns a <= 1'b1;
			#50ns b <= 1'b0;
			#50ns a <= 1'b0;
			#50ns b <= 1'b1;
		end
		repeat (15) begin
			#50ns b <= 1'b0;
			#50ns a <= 1'b1;
			#50ns b <= 1'b1;
			#50ns a <= 1'b0;
		end
		$stop;
	end

	encoder #(
		.MAX(MAX),
		.DEBONUCE_N(DEB)
	) dut (
		.clk(clk),
		.rst(rst),
		.a(a),
		.b(b),
		.x()
	);
endmodule

`default_nettype wire
