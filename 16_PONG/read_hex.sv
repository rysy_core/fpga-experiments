/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module read_hex;
    initial begin
        int f, r;
        logic [7:0]b;
        f = $fopen("test.bmp", "rb");
        while   ($fread(b, f))
            $display("%x", b);
        $fclose(f);
    end
endmodule

`default_nettype wire
