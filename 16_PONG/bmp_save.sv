/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module bmp_save;

    initial begin
        int width, height;
        logic [2:0][7:0]c[][];

        width = 4;
        height = 3;

        c = new[height];
        for (int y = 0; y < height; y++)
            c[y] = new[width];
        
        c[0][0] = {8'd255, 8'd0, 8'd0};
        c[0][1] = {8'd0, 8'd255, 8'd0};
        c[0][2] = {8'd0, 8'd0, 8'd255};
        c[0][3] = {8'd255, 8'd255, 8'd255};

        c[1][0] = {8'd255, 8'd255, 8'd255};
        c[1][1] = {8'd0, 8'd0, 8'd0};
        c[1][2] = {8'd0, 8'd0, 8'd0};
        c[1][3] = {8'd255, 8'd255, 8'd255};

        c[2][0] = {8'd255, 8'd255, 8'd255};
        c[2][1] = {8'd0, 8'd0, 8'd0};
        c[2][2] = {8'd0, 8'd0, 8'd0};
        c[2][3] = {8'd255, 8'd255, 8'd255};

        bmpPkg::save("save_test.bmp", width, height, c);
    end
endmodule
 
`default_nettype wire
