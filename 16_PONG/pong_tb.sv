/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2021 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module pong_tb;
    parameter H = 800;
    parameter V = 525;
    parameter H_BIT = $clog2(H);
    parameter V_BIT = $clog2(V);

	logic clk, rst, ce;
    logic button;
    logic [V_BIT-1:0]encoder;

	initial begin
		clk <= '0;
		forever #5ns clk = ~clk;
	end

    initial begin
        rst <= '0;
        repeat(2)
            @(posedge clk);
        rst <= '1;
    end

    initial begin
        ce <= 1'b0;
        @(posedge clk);
        forever begin
            ce <= 1'b1;
            @(posedge clk);
            ce <= 1'b0;
            repeat(3) @(posedge clk);
        end
    end

    initial begin
        button = '0;
        encoder = '0;
        repeat(10) @(posedge clk);

        button = '1;
        while (dut.state != pongPkg::PONG)
            @(posedge clk);
        button = '0;

        for (int i=0; i<4000; i++) begin
            if (dut.ball_y < 30)
                encoder = 0;
            else if (dut.ball_y > 429)
                encoder = 400;
            else
                encoder = dut.ball_y-30;
            @(posedge clk);
        end

        button = '1;
        while (dut.state != pongPkg::PONG)
            @(posedge clk);
        button = '0;
      
        repeat(500) @(posedge clk);
        $stop;        
    end

    pong dut (
        .clk(clk),
        .rst(rst),
        .ce(ce),
        .button(button),
        .encoder(encoder),
        .ball_x(),
        .ball_y(),
        .opp_y(),
        .state());

endmodule

`default_nettype wire
