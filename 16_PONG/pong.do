#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog pong_pkg.sv
vlog opponent.sv
vlog ball.sv
vlog pong.sv
vlog pong_tb.sv

vsim work.pong_tb

add wave -position end sim:/pong_tb/clk
add wave -position end sim:/pong_tb/rst
add wave -divider input
add wave -position end sim:/pong_tb/ce
add wave -position end sim:/pong_tb/button
add wave -position end -unsigned sim:/pong_tb/encoder
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 480 sim:/pong_tb/encoder

add wave -divider pong
add wave -position end -unsigned sim:/pong_tb/dut/reflection
add wave -position end -unsigned sim:/pong_tb/dut/new_game
add wave -position end -unsigned sim:/pong_tb/dut/state
add wave -position end -unsigned sim:/pong_tb/dut/ball_x
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 640 sim:/pong_tb/dut/ball_x
add wave -position end -unsigned sim:/pong_tb/dut/ball_y
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 480 sim:/pong_tb/dut/ball_y
add wave -position end -unsigned sim:/pong_tb/dut/opp_y
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 480 sim:/pong_tb/dut/opp_y

run -all
wave zoom full
