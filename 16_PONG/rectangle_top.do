#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

set QUARTUS_DIR "C:/intelFPGA_lite/18.1/quartus/eda/sim_lib/"
set QUARTUS_DIR "/home/rafal/intelFPGA_lite/20.1/quartus/eda/sim_lib"
vlib work

vlog $QUARTUS_DIR/altera_mf.v

vlog ../15_VGA/vga_pkg.sv
vlog ../02_counter/counter.sv
vlog ../03_shiftreg/edge_detector.sv
vlog ../03_shiftreg/debounce.sv
vlog encoder.sv
vlog ../15_VGA/vga.sv
vlog ../15_VGA/pll.v
vlog bmp_pkg.sv
vlog vga_monitor.sv
vlog rectangle.sv
vlog rectangle_top.sv
vlog rectangle_top_tb.sv

vsim work.rectangle_top_tb

vsim work.rectangle_top_tb

add wave -position end sim:/rectangle_top_tb/clk
add wave -position end sim:/rectangle_top_tb/rst
add wave -position end sim:/rectangle_top_tb/dut/clk_vga
add wave -position end sim:/rectangle_top_tb/ea
add wave -position end sim:/rectangle_top_tb/eb

add wave -divider out
add wave -position end -unsigned sim:/rectangle_top_tb/dut/py
add wave -position end -unsigned sim:/rectangle_top_tb/dut/rect/py_t
add wave -position end sim:/rectangle_top_tb/dut/vga

add wave -divider vga_monitor

add wave -position end sim:/rectangle_top_tb/monitor/x
add wave -position end sim:/rectangle_top_tb/monitor/y

run -all
wave zoom full
